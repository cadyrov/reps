#!/bin/bash
FROM golang:1.19-alpine as builder

WORKDIR /reports
COPY . /reports
RUN apk add git
RUN apk add curl
RUN apk add make
ENV GO111MODULE=on
RUN go mod tidy
RUN make build

FROM alpine:latest as runner
EXPOSE $REPORTS_WEB_PORT
EXPOSE 9000
COPY --from=builder /reports /reports/
WORKDIR /reports/
ENTRYPOINT ["bin/srv"]