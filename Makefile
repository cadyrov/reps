build:
	rm ./bin/srv; \
	go build -o ./bin/srv ./cmd/http/main.go
swagger:
	swag init --parseDependency
lint:
	golangci-lint run