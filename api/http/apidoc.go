package http

// @Version 1.0.0
// @Title Backend API for NN-techcon-reports
// @Description API usually works as expected
// @ContactEmail cadyrov@email.com
// @LicenseName MIT
// @LicenseURL https://en.wikipedia.org/wiki/MIT_License
// @Server http://www.fake.com Server-1
// @Security AuthorizationHeader read write
// @SecurityScheme AuthorizationHeader http bearer Input your token
