package handlers

import (
	"github.com/rs/zerolog"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/internal/service"
)

type App struct {
	Logger  zerolog.Logger
	Config  domain.Config
	Service *service.App
}
