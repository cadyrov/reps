package handlers

import (
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/domain"
)

// ClientCreate godoc
// @Summary      Создание клиента
// @Description  Создание клиента
// @Security     BearerAuth
// @Tags         Client
// @Accept       json
// @Produce      json
// @Param object  body  domain.ClientForm  true  "JSON"
// @Success      200  {object}  domain.Client
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/client  [POST].
func ClientCreate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.ClientForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		cl, errs := app.Service.ClientCreate(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "create_client", cl, nil)
	}
}

// ClientUpdate godoc
// @Summary      Обновление клиента
// @Description  Обновление клиента
// @Security     BearerAuth
// @Tags         Client
// @Accept       json
// @Produce      json
// @Param client_id  path  int  true  "ID клиента."
// @Param object  body  domain.ClientForm  true  "JSON"
// @Success      200  {object}  domain.Client
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/client/{client_id}  [PATCH].
func ClientUpdate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		clientID, err := strconv.Atoi(mux.Vars(r)["client_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.ClientForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, errs := app.Service.ClientUpdate(r.Context(), clientID, frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "update client", str, nil)
	}
}

// ClientDelete godoc
// @Summary      Удаление клиента
// @Description  Удаление клиента
// @Security     BearerAuth
// @Tags         Client
// @Accept       json
// @Produce      json
// @Param client_id  path  int  true  "ID отчета."
// @Success      204
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/client/{client_id}  [DELETE].
func ClientDelete(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		clientID, err := strconv.Atoi(mux.Vars(r)["client_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		errs := app.Service.ClientDelete(r.Context(), clientID)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "delete client", nil, nil)
	}
}

// ClientSearch godoc
// @Summary      Создание клиента
// @Description  Создание клиента
// @Security     BearerAuth
// @Tags         Client
// @Accept       json
// @Produce      json
// @Param object  body  domain.ClientLookupForm  true  "JSON"
// @Success      200  {object}  domain.Clients
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/client  [POST].
func ClientSearch(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.ClientLookupForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, pg, errs := app.Service.ClientSearch(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "search client", str, pg)
	}
}
