package handlers

import (
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/domain"
)

// ClimateCenterCreate godoc
// @Summary      Создание климатического центра
// @Description  Создание климатического центра
// @Security     BearerAuth
// @Tags         Climate Center
// @Accept       json
// @Produce      json
// @Param object  body  domain.ClimateCenterForm  true  "JSON"
// @Success      200  {object}  domain.ClimateCenter
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/climate_center  [POST].
func ClimateCenterCreate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.ClimateCenterForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		cl, errs := app.Service.ClimateCenterCreate(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "create_climate_center", cl, nil)
	}
}

// ClimateCenterUpdate godoc
// @Summary      Обновление климатического центра
// @Description  Обновление климатического центра
// @Security     BearerAuth
// @Tags         Climate Center
// @Accept       json
// @Produce      json
// @Param climate_center_id  path  int  true  "ID."
// @Param object  body  domain.ClimateCenterForm  true  "JSON"
// @Success      200  {object}  domain.ClimateCenter
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/climate_center/{climate_center_id}  [PATCH].
func ClimateCenterUpdate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		climateCenterID, err := strconv.Atoi(mux.Vars(r)["climate_center_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.ClimateCenterForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, errs := app.Service.ClimateCenterUpdate(r.Context(), climateCenterID, frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "update climate_center", str, nil)
	}
}

// ClimateCenterDelete godoc
// @Summary      Удаление климатического центра
// @Description  Удаление климатического центра
// @Security     BearerAuth
// @Tags         Climate Center
// @Accept       json
// @Produce      json
// @Param climate_center_id  path  int  true  "ID отчета."
// @Success      204
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/climate_center/{climate_center_id}  [DELETE].
func ClimateCenterDelete(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		climateCenterID, err := strconv.Atoi(mux.Vars(r)["climate_center_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		errs := app.Service.ClimateCenterDelete(r.Context(), climateCenterID)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "delete climate_center", nil, nil)
	}
}

// ClimateCenterSearch godoc
// @Summary      Поиск климатического центра
// @Description  Поиск климатического центра
// @Security     BearerAuth
// @Tags         Climate Center
// @Accept       json
// @Produce      json
// @Param object  body  domain.ClimateCenterLookupForm  true  "JSON"
// @Success      200  {object}  domain.ClimateCenters
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/climate_center  [POST].
func ClimateCenterSearch(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.ClimateCenterLookupForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, pg, errs := app.Service.ClimateCenterSearch(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "search climate_center", str, pg)
	}
}
