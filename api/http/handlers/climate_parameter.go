package handlers

import (
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/domain"
)

// ClimateParameterCreate godoc
// @Summary      Создание климатических параметров
// @Description  Создание климатических параметров
// @Security     BearerAuth
// @Tags         Climate Parameter
// @Accept       json
// @Produce      json
// @Param object  body  domain.ClimateParameterForm  true  "JSON"
// @Success      200  {object}  domain.ClimateParameter
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/climate_parameter  [POST].
func ClimateParameterCreate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.ClimateParameterForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		cl, errs := app.Service.ClimateParameterCreate(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "create_climate_parameter", cl, nil)
	}
}

// ClimateParameterUpdate godoc
// @Summary      Обновление климатических параметров
// @Description  Обновление климатических параметров
// @Security     BearerAuth
// @Tags         Climate Parameter
// @Accept       json
// @Produce      json
// @Param climate_parameter_id  path  int  true  "ID."
// @Param object  body  domain.ClimateParameterForm  true  "JSON"
// @Success      200  {object}  domain.ClimateParameter
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/climate_parameter/{climate_parameter_id}  [PATCH].
func ClimateParameterUpdate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		climateParameterID, err := strconv.Atoi(mux.Vars(r)["climate_parameter_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.ClimateParameterForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, errs := app.Service.ClimateParameterUpdate(r.Context(), climateParameterID, frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "update climate_parameter", str, nil)
	}
}

// ClimateParameterDelete godoc
// @Summary      Удаление климатических параметров
// @Description  Удаление климатических параметров
// @Security     BearerAuth
// @Tags         Climate Parameter
// @Accept       json
// @Produce      json
// @Param climate_parameter_id  path  int  true  "ID отчета."
// @Success      204
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/climate_parameter/{climate_parameter_id}  [DELETE].
func ClimateParameterDelete(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		climateParameterID, err := strconv.Atoi(mux.Vars(r)["climate_parameter_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		errs := app.Service.ClimateParameterDelete(r.Context(), climateParameterID)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "delete climate_parameter", nil, nil)
	}
}

// ClimateParameterSearch godoc
// @Summary      Поиск климатических параметров
// @Description  Поиск климатических параметров
// @Security     BearerAuth
// @Tags         Climate Parameter
// @Accept       json
// @Produce      json
// @Param object  body  domain.ClimateParameterLookupForm  true  "JSON"
// @Success      200  {object}  domain.ClimateParameters
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/climate_parameter  [POST].
func ClimateParameterSearch(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.ClimateParameterLookupForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, pg, errs := app.Service.ClimateParameterSearch(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "search climate_parameter", str, pg)
	}
}
