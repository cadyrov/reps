package handlers

import (
	"context"
	"io"
	"log"
	"net/http"
	"runtime/debug"
	"strings"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/getsentry/sentry-go"
	"gitlab.com/techconnn/reports/domain"
)

type SwaggerReply struct {
	Data string `json:"data"`
}

type Error struct {
	Message string `json:"error"`
}

func (app *App) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reqToken := r.Header.Get("Authorization")
		splitToken := strings.ReplaceAll(reqToken, "Bearer ", "")

		ur, err := app.Service.Validate(r.Context(), splitToken)
		if err != nil {
			e := goerr.New("Check authorization error:" + err.Error()).HTTP(http.StatusUnauthorized)

			godict.SendError(w, e)

			return
		}

		httpContext := context.WithValue(r.Context(), domain.ContextKeyUserRender, ur) //nolint:staticcheck

		next.ServeHTTP(w, r.WithContext(httpContext))
	})
}

func corsHeaders(responseWriter http.ResponseWriter, corsHeader string) {
	responseWriter.Header().Set("Access-Control-Allow-Origin", corsHeader)
	responseWriter.Header().Set("Access-Control-Allow-Credentials", "true")
	responseWriter.Header().Set("Access-Control-Allow-Methods", "GET,POST,PATCH,PUT,UPDATE,DELETE,OPTIONS")
	responseWriter.Header().Set("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Key, Authorization, Cookie")
}

func (app *App) CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(responseWriter http.ResponseWriter, request *http.Request) {
		origin := checkCorsHeaders(app, request)

		corsHeaders(responseWriter, origin)

		next.ServeHTTP(responseWriter, request)
	})
}

func checkCorsHeaders(app *App, request *http.Request) string {
	corses := strings.Split(app.Config.Cors, ",")

	origin := request.Header.Get("Origin")

	var corsHeader string

	for i := range corses {
		if strings.Contains(corses[i], origin) {
			corsHeader = corses[i]

			return corsHeader
		}
	}

	return corsHeader
}

// Метод не доступен.
func (app *App) NotAllowedMiddleware(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		origin := checkCorsHeaders(app, r)

		corsHeaders(w, origin)

		return
	}

	e := goerr.New("requested method is not allowed").HTTP(http.StatusMethodNotAllowed)

	godict.Send(w, godict.Error(e))
}

func (app *App) LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				log.Println(string(debug.Stack()))
			}
		}()
		next.ServeHTTP(w, r)
	})
}

// nolint:unused
func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func NotFoundHandler(config domain.Config) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		client := http.DefaultClient

		rq, err := http.NewRequestWithContext(r.Context(), r.Method, config.PhpURI+r.RequestURI, r.Body)
		if err != nil {
			http.Error(w, "Server Error", http.StatusInternalServerError)

			return
		}

		rq.Header = r.Header

		resp, err := client.Do(rq)
		if err != nil {
			http.Error(w, "Server Error", http.StatusInternalServerError)

			return
		}
		defer resp.Body.Close()

		w.WriteHeader(resp.StatusCode)

		if _, err := io.Copy(w, resp.Body); err != nil {
			http.Error(w, "Server Error", http.StatusInternalServerError)
		}
	})
}

func sendError(w http.ResponseWriter, e goerr.IError) {
	if e.GetCode() != http.StatusBadRequest &&
		e.GetCode() != http.StatusUnauthorized {
		sentry.CaptureException(e)
	}

	godict.SendError(w, e)
}

func sendOK(writer http.ResponseWriter, message interface{}, data interface{},
	pagination interface{}) {
	godict.SendOk(writer, message, data, pagination)
}

func parseBody(r io.ReadCloser, data interface{}) goerr.IError {
	return godict.ParseBody(r, &data)
}
