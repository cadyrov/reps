package handlers

import (
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/domain"
)

// CompanyCreate godoc
// @Summary      Создание компаний
// @Description  Создание компаний
// @Security     BearerAuth
// @Tags         Company
// @Accept       json
// @Produce      json
// @Param object  body  domain.CompanyForm  true  "JSON"
// @Success      200  {object}  domain.Company
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/company  [POST].
func CompanyCreate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.CompanyForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		cl, errs := app.Service.CompanyCreate(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "create_company", cl, nil)
	}
}

// CompanyUpdate godoc
// @Summary      Обновление компаний
// @Description  Обновление компаний
// @Security     BearerAuth
// @Tags         Company
// @Accept       json
// @Produce      json
// @Param company_id  path  int  true  "ID."
// @Param object  body  domain.CompanyForm  true  "JSON"
// @Success      200  {object}  domain.Company
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/company/{company_id}  [PATCH].
func CompanyUpdate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		companyID, err := strconv.Atoi(mux.Vars(r)["company_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.CompanyForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, errs := app.Service.CompanyUpdate(r.Context(), companyID, frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "update company", str, nil)
	}
}

// CompanyDelete godoc
// @Summary      Удаление компаний
// @Description  Удаление компаний
// @Security     BearerAuth
// @Tags         Company
// @Accept       json
// @Produce      json
// @Param company_id  path  int  true  "ID отчета."
// @Success      204
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/company/{company_id}  [DELETE].
func CompanyDelete(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		companyID, err := strconv.Atoi(mux.Vars(r)["company_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		errs := app.Service.CompanyDelete(r.Context(), companyID)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "delete company", nil, nil)
	}
}

// CompanySearch godoc
// @Summary      Поиск компаний
// @Description  Поиск компаний
// @Security     BearerAuth
// @Tags         Company
// @Accept       json
// @Produce      json
// @Param object  body  domain.CompanyLookupForm  true  "JSON"
// @Success      200  {object}  domain.Companys
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/company  [POST].
func CompanySearch(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.CompanyLookupForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, pg, errs := app.Service.CompanySearch(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "search company", str, pg)
	}
}
