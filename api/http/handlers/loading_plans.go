package handlers

import (
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/domain"
)

// LoadingPlanCreate godoc
// @Summary      Создание планов
// @Description  Создание планов
// @Security     BearerAuth
// @Tags         LoadingPlan
// @Accept       json
// @Produce      json
// @Param object  body  domain.LoadingPlanForm  true  "JSON"
// @Success      200  {object}  domain.LoadingPlan
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/loading_plan  [POST].
func LoadingPlanCreate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.LoadingPlanForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		cl, errs := app.Service.LoadingPlanCreate(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "create_loading_plan", cl, nil)
	}
}

// LoadingPlanUpdate godoc
// @Summary      Обновление планов
// @Description  Обновление планов
// @Security     BearerAuth
// @Tags         LoadingPlan
// @Accept       json
// @Produce      json
// @Param loading_plans_id  path  int  true  "ID."
// @Param object  body  domain.LoadingPlanForm  true  "JSON"
// @Success      200  {object}  domain.LoadingPlans
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/loading_plans/{loading_plans_id}  [PATCH].
func LoadingPlanUpdate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		loadingPlansID, err := strconv.Atoi(mux.Vars(r)["loading_plans_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.LoadingPlanForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, errs := app.Service.LoadingPlanUpdate(r.Context(), loadingPlansID, frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "update loading_plans", str, nil)
	}
}

// LoadingPlanDelete godoc
// @Summary      Удаление планов
// @Description  Удаление планов
// @Security     BearerAuth
// @Tags         LoadingPlan
// @Accept       json
// @Produce      json
// @Param loading_plans_id  path  int  true  "ID отчета."
// @Success      204
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/loading_plans/{loading_plans_id}  [DELETE].
func LoadingPlanDelete(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		loadingPlanID, err := strconv.Atoi(mux.Vars(r)["loading_plans_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		errs := app.Service.LoadingPlanDelete(r.Context(), loadingPlanID)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "delete loading_plans", nil, nil)
	}
}

// LoadingPlanSearch godoc
// @Summary      Поиск планов
// @Description  Поиск планов
// @Security     BearerAuth
// @Tags         LoadingPlan
// @Accept       json
// @Produce      json
// @Param object  body  domain.LoadingPlanLookupForm  true  "JSON"
// @Success      200  {object}  domain.LoadingPlans
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/loading_plans  [POST].
func LoadingPlanSearch(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.LoadingPlanLookupForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, pg, errs := app.Service.LoadingPlansLookup(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "search loading_plans", str, pg)
	}
}
