package handlers

import (
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/domain"
)

// ObjectDestinationSearch godoc
// @Summary      Поиск назначение объекта
// @Description  Только неудаленные объекты
// @Security     BearerAuth
// @Tags         ObjectDestination
// @Accept       json
// @Produce      json
// @Param object  body  domain.ObjectDestinationSearchForm  true  "форма поиска"
// @Success      200  {object}  domain.ObjectDestinations
// @Failure      401  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/object_destinations/search  [POST].
func ObjectDestinationSearch(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.ObjectDestinationSearchForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, pg, errs := app.Service.ObjectDestinationSearch(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "object destination search", str, pg)
	}
}

// ObjectDestinationCreate godoc
// @Summary      Создание назначения объекта
// @Security     BearerAuth
// @Tags         ObjectDestination
// @Accept       json
// @Produce      json
// @Param object  body  domain.ObjectDestinationForm  true  "форма создания"
// @Success      200  {object}  domain.ObjectDestination
// @Failure      401  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/object_destinations  [POST].
func ObjectDestinationCreate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.ObjectDestinationForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		str, errs := app.Service.ObjectDestinationCreate(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "object destination search", str, nil)
	}
}

// ObjectDestinationUpdate godoc
// @Summary      Обновление объекта назначения
// @Description  Обновление объекта назначения
// @Security     BearerAuth
// @Tags         ObjectDestination
// @Accept       json
// @Produce      json
// @Param destination_id  path  int  true  "ID объекта."
// @Param object  body  domain.ObjectDestinationForm  true  "форма обновления"
// @Success      200  {object}  domain.ObjectDestination
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/object_destinations/{destination_id} [PATCH].
func ObjectDestinationUpdate(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		objectID, err := strconv.Atoi(mux.Vars(r)["destination_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.ObjectDestinationForm{}
		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		od, errs := app.Service.ObjectDestinationUpdate(r.Context(), objectID, frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "object destination upsert", od, nil)
	}
}

// ObjectDestinationDelete godoc
// @Summary      Удаление объекта назначения
// @Description  Удаление объекта назначения
// @Security     BearerAuth
// @Tags         ObjectDestination
// @Accept       json
// @Produce      json
// @Param destination_id  path  int  true  "ID объекта."
// @Success      200  {object}  SwaggerReply
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/object_destinations/{destination_id} [DELETE].
func ObjectDestinationDelete(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		objectID, err := strconv.Atoi(mux.Vars(r)["destination_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.ObjectDestinationForm{}
		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		if errs := app.Service.ObjectDestinationDelete(r.Context(), objectID); errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "object destination delete", nil, nil)
	}
}
