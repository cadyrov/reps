package handlers

import (
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

// ReportFileUpload godoc
// @Summary      Загрузка файла
// @Description  Получение ссылки для загрузки файла на S3 хранилище Общие файлы отчета - файлы привязанные к отчету
// @Description  Хрaнятся на s3  ReportID/trash/...
// @Description  https://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_AmazonS3_PutObject.htm
// @Description  https://aws.amazon.com/ru/blogs/rus/uploading-to-amazon-s3-directly-from-a-web-or-mobile-application
// @Description  Объем загружаемых файлов для каждого отчета независимо от количество вложенных папок ограничен
// @Description  Вычисление дейтвующего объема происходит после загрузки файлов. поскольку операция разорвана во времени
// @Description  Существует возможность обхода ограничения созданием большого числа ссылок
// @Description  без фактической загрузки и далее их использование пачкой
// @Security     BearerAuth
// @Tags         Reports
// @Accept       json
// @Produce      json
// @Param report_id  path  int  true  "ID отчета."
// @Param object  body  domain.FileUploadForm  true  "JSON"
// @Success      200  {object}  SwaggerReply
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      409  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/reports/{report_id}/file/upload  [POST].
func ReportFileUpload(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		reportID, err := strconv.ParseInt(mux.Vars(r)["report_id"], 10, 64) //nolint:gomnd
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.FileUploadForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		frm.TTL = domain.FileUploadLinkTTL

		str, errs := app.Service.ReportUploadFile(r.Context(), reportID, frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "getUploadLink", str, nil)
	}
}

// ReportFileList godoc
// @Summary      Список общие файлы отчета
// @Description  Хрaнятся на s3  ReportID/trash/...
// @Security     BearerAuth
// @Tags         Reports
// @Accept       json
// @Produce      json
// @Param report_id  path  int  true  "ID отчета."
// @Param path  query  string  true  "относительная маска пути"
// @Success      200  {object}  SwaggerReply
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/reports/{report_id}/file  [GET].
func ReportFileList(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		reportID, err := strconv.ParseInt(mux.Vars(r)["report_id"], 10, 64) //nolint:gomnd
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		rsp, errs := app.Service.ReportListFile(r.Context(), reportID, r.URL.Query().Get("path"))
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "file list", rsp, nil)
	}
}

// ReportFileDrop godoc
// @Summary      Удаление общие файлы отчета
// @Description  В качестве пути используется относительный путь от ReportID/trash
// @Description  В самом простом случае - название файла
// @Security     BearerAuth
// @Tags         Reports
// @Accept       json
// @Produce      json
// @Param report_id  path  int  true  "ID отчета."
// @Param object  body  domain.DropFileForm  true  "JSON"
// @Success      200  {object}  SwaggerReply
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/reports/{report_id}/file/drop [POST].
func ReportFileDrop(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		reportID, err := strconv.ParseInt(mux.Vars(r)["report_id"], 10, 64) //nolint:gomnd
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.DropFileForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			godict.Send(w, godict.Error(err))

			return
		}

		if errs := app.Service.ReportDropFile(r.Context(), reportID, frm.Path); errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "file drop", nil, nil)
	}
}

// ReportsSearch godoc
// @Summary      поиск по отчетам
// @Description  Обязательная фильтрация по аккаунту пользователя
// @Security     BearerAuth
// @Tags         Reports
// @Tags         Mobile
// @Accept       json
// @Produce      json
// @Param object  body  domain.ReportSearchForm  true  "JSON"
// @Success      200  {object}  domain.Reports
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/reports [POST].
func ReportsSearch(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.ReportSearchForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			sendError(w, err)

			return
		}

		dmn, pg, err := app.Service.ReportLookup(r.Context(), frm)
		if err != nil {
			sendError(w, err)

			return
		}

		sendOK(w, "report lookup", dmn, pg)
	}
}

// ReportsConstructions godoc
// @Summary      Конструкции
// @Description  Получение конструкций, привязанных к отчету. поисх проходит только по тому же аккаунту
// на котором пользователь
// @Security     BearerAuth
// @Tags         Reports
// @Tags         Mobile
// @Accept       json
// @Produce      json
// @Param report_id  path  integer  true  "ID отчета"
// @Param json  body  domain.ConstructionLookupForm  true  "ID отчета"
// @Success      200  {object}  domain.Constructions
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/reports/{report_id}/constructions [POST].
func ReportsConstructions(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.ConstructionLookupForm{}

		id, err := strconv.Atoi(mux.Vars(r)["report_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		if err := parseBody(r.Body, &frm); err != nil {
			sendError(w, err)

			return
		}

		frm.ReportID = id

		dmn, pg, errs := app.Service.ConstructionLookup(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "report construction lookup", dmn, pg)
	}
}

// ReportsPlans godoc
// @Summary      Планы отчета
// @Description  Получение планов, привязанных к отчету
// @Security     BearerAuth
// @Tags         Reports
// @Tags         Mobile
// @Accept       json
// @Produce      json
// @Param report_id  path  integer  true  "ID отчета"
// @Param with_image  query  bool  false  "ограничение отдачи по изображениям"
// @Success      200  {object}  domain.LoadingPlans
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/reports/{report_id}/plans [GET].
func ReportsPlans(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		frm := domain.LoadingPlanLookupForm{}

		id, err := strconv.Atoi(mux.Vars(r)["report_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm.ReportID = id

		switch {
		case r.URL.Query().Get("with_image") == "true":
			frm.WithImage = utils.Ptr(true)
		case r.URL.Query().Get("with_image") == "false":
			frm.WithImage = utils.Ptr(false)
		}

		dmn, pg, errs := app.Service.LoadingPlansLookup(r.Context(), frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "report loading plan", dmn, pg)
	}
}

// ReportUploadPhoto godoc
// @Summary      Загрузка фото
// @Description  Загрузка фото в отчетов
// @Description  Получение ссылки для загрузки файла на S3 хранилище  файлы привязанные к отчету
// @Description  https://habr.com/ru/articles/701736/
// @Description  https://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_AmazonS3_PutObject.htm
// @Description  https://aws.amazon.com/ru/blogs/rus/uploading-to-amazon-s3-directly-from-a-web-or-mobile-application
// @Description  Фото загружается только в отчет со статусом domain.ReportStatePhoto
// @Security     BearerAuth
// @Tags         Reports
// @Tags         Mobile
// @Accept       json
// @Produce      json
// @Param report_id  path  integer  true  "ID отчета"
// @Param object  body  domain.UploadPhotoForm  true  "JSON"
// @Success      200  {object}  domain.UploadPhotoRender
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/reports/{report_id}/uploadphoto [POST].
func ReportUploadPhoto(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.Atoi(mux.Vars(r)["report_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.UploadPhotoForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		rnd, errs := app.Service.ReportUploadPhoto(r.Context(), id, frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "upload photo", rnd, nil)
	}
}

// ReportBlocks godoc
// @Summary      Блоки отчета
// @Description  Возвращает блоки привязаннх к отчету конструкций
// @Security     BearerAuth
// @Tags         Reports
// @Tags         Mobile
// @Accept       json
// @Produce      json
// @Param report_id  path  integer  true  "ID отчета"
// @Success      200  {object}  domain.Blocks
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/reports/{report_id}/blocks [POST].
func ReportBlocks(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.Atoi(mux.Vars(r)["report_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		lst, err := app.Service.ReportBlocks(r.Context(), id)
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		sendOK(w, "report blocks", lst, nil)
	}
}

// ReportUploadPlan godoc
// @Summary      Загрузка планов
// @Description  Загрузка планов в отчеты
// @Description  Получение ссылки для загрузки файла на S3 хранилище  файлы привязанные к планам отчета
// @Description  storage.bucket/{ReportID}/plans/{PlanID}/filename.jpg
// @Description  https://habr.com/ru/articles/701736/
// @Description  https://docs.aws.amazon.com/sdkfornet1/latest/apidocs/html/M_Amazon_S3_AmazonS3_PutObject.htm
// @Description  https://aws.amazon.com/ru/blogs/rus/uploading-to-amazon-s3-directly-from-a-web-or-mobile-application
// @Security     BearerAuth
// @Tags         Reports
// @Accept       json
// @Produce      json
// @Param report_id  path  integer  true  "ID отчета"
// @Param object  body  domain.UploadPlanPhotoForm  true  "JSON"
// @Success      200  {object}  domain.UploadPlanPhotoRender
// @Failure      401  {object}  Error
// @Failure      403  {object}  Error
// @Failure      404  {object}  Error
// @Failure      500  {object}  Error
// @Router       /v2/reports/{report_id}/uploadplan [POST].
func ReportUploadPlan(app *App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.Atoi(mux.Vars(r)["report_id"])
		if err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		frm := domain.UploadPlanPhotoForm{}

		if err := parseBody(r.Body, &frm); err != nil {
			sendError(w, goerr.New(err.Error()))

			return
		}

		rnd, errs := app.Service.ReportUploadPlan(r.Context(), id, frm)
		if errs != nil {
			sendError(w, errs)

			return
		}

		sendOK(w, "upload photo", rnd, nil)
	}
}
