package handlers

import (
	"net/http"

	"github.com/cadyrov/godict"
	"gitlab.com/techconnn/reports"
	"gitlab.com/techconnn/reports/domain"
)

// HealthCheck godoc
// @Summary      Проверка сервера
// @Description  Проверка сервера
// @Tags         System
// @Accept       json
// @Produce      json
// @Success      200  {object}  SwaggerReply
// @Router       /system/health [GET].
func HealthCheck() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		godict.Send(w, godict.Ok("Alive", nil, nil))
	}
}

// Swagger godoc
// @Summary      получить документацию
// @Description  получить документацию
// @Tags         System
// @Accept       json
// @Produce      json
// @Success      200  {object}  SwaggerReply
// @Failure      500  {object}  Error
// @Router       /system/swagger [GET].
func Swagger() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		sw := reports.Swagger
		if _, err := w.Write(sw); err != nil {
			godict.SendError(w, domain.IntErr(err))
		}
	}
}
