package router

import (
	"context"
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"gitlab.com/hof4eg/auth_client"
	"gitlab.com/techconnn/reports/api/http/handlers"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/internal/service"
	"gitlab.com/techconnn/reports/internal/storage/psql"
)

type App struct {
	*handlers.App
}

func New(ctx context.Context, l zerolog.Logger, c domain.Config) *App {
	repo, err := psql.New(c.PSQL.DSN, c.PSQL.ReadTimeout, c.PSQL.WriteTimeout, &l)
	if err != nil {
		panic(err)
	}

	auri, err := url.Parse(c.Auth.URI)
	if err != nil {
		panic(err)
	}

	authClient, err := auth_client.New(*auri, c.Auth.Timeout, c.Auth.XServerToken)
	if err != nil {
		panic(err)
	}

	cr := service.New(ctx, c, repo, l, authClient)

	return &App{
		&handlers.App{
			Logger:  l,
			Config:  c,
			Service: cr,
		},
	}
}

func (app *App) GetRoutes() *mux.Router {
	routes := mux.NewRouter()
	routes.Use(app.LoggingMiddleware)
	routes.Use(app.CorsMiddleware)
	routes.MethodNotAllowedHandler = http.HandlerFunc(app.NotAllowedMiddleware)
	routes.NotFoundHandler = app.CorsMiddleware(handlers.NotFoundHandler(app.Config))

	mainRoute := routes.PathPrefix("/api").Subrouter()

	system := mainRoute.PathPrefix("/system").Subrouter()
	app.SetSystemRoutes(system)

	v2Route := mainRoute.PathPrefix("/v2").Subrouter()

	reportsRoute := v2Route.PathPrefix("/reports").Subrouter()
	app.SetReportsRoutes(reportsRoute)
	reportsRoute.Use(app.AuthMiddleware)

	clientRoute := v2Route.PathPrefix("/client").Subrouter()
	app.SetClientRoutes(clientRoute)
	clientRoute.Use(app.AuthMiddleware)

	objectDestRoute := v2Route.PathPrefix("/object_destination").Subrouter()
	app.SetObjectDestinationRoutes(objectDestRoute)
	objectDestRoute.Use(app.AuthMiddleware)

	climateCenterRoute := v2Route.PathPrefix("/climate_center").Subrouter()
	app.SetClimateCenterRoutes(climateCenterRoute)
	climateCenterRoute.Use(app.AuthMiddleware)

	climateParameterRoute := v2Route.PathPrefix("/climate_parameter").Subrouter()
	app.SetClimateParameterRoutes(climateParameterRoute)
	climateParameterRoute.Use(app.AuthMiddleware)

	companyRoute := v2Route.PathPrefix("/company").Subrouter()
	app.SetCompanyRoutes(companyRoute)
	companyRoute.Use(app.AuthMiddleware)

	loadingPlanRoute := v2Route.PathPrefix("/loading_plan").Subrouter()
	app.SetLoadingPlanRoutes(loadingPlanRoute)
	loadingPlanRoute.Use(app.AuthMiddleware)

	return routes
}
