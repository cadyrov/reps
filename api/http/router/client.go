package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/api/http/handlers"
)

func (app *App) SetClientRoutes(router *mux.Router) {
	router.HandleFunc("", handlers.ClientCreate(app.App)).Methods(http.MethodPost)
	router.HandleFunc("/{client_id:[0-9]+}", handlers.ClientUpdate(app.App)).Methods(http.MethodPatch)
	router.HandleFunc("/{client_id:[0-9]+}", handlers.ClientDelete(app.App)).Methods(http.MethodDelete)
	router.HandleFunc("/search", handlers.ClientSearch(app.App)).Methods(http.MethodPost)
}
