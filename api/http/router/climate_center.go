package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/api/http/handlers"
)

func (app *App) SetClimateCenterRoutes(router *mux.Router) {
	router.HandleFunc("", handlers.ClimateCenterCreate(app.App)).Methods(http.MethodPost)
	router.HandleFunc("/{climate_center_id:[0-9]+}",
		handlers.ClimateCenterUpdate(app.App)).Methods(http.MethodPatch)
	router.HandleFunc("/{climate_center_id:[0-9]+}",
		handlers.ClimateCenterDelete(app.App)).Methods(http.MethodDelete)
	router.HandleFunc("/search", handlers.ClimateCenterSearch(app.App)).Methods(http.MethodPost)
}
