package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/api/http/handlers"
)

func (app *App) SetClimateParameterRoutes(router *mux.Router) {
	router.HandleFunc("", handlers.ClimateParameterCreate(app.App)).Methods(http.MethodPost)
	router.HandleFunc("/{climate_parameter_id:[0-9]+}",
		handlers.ClimateParameterUpdate(app.App)).Methods(http.MethodPatch)
	router.HandleFunc("/{climate_parameter_id:[0-9]+}",
		handlers.ClimateParameterDelete(app.App)).Methods(http.MethodDelete)
	router.HandleFunc("/search", handlers.ClimateParameterSearch(app.App)).Methods(http.MethodPost)
}
