package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/api/http/handlers"
)

func (app *App) SetCompanyRoutes(router *mux.Router) {
	router.HandleFunc("", handlers.CompanyCreate(app.App)).Methods(http.MethodPost)
	router.HandleFunc("/{company_id:[0-9]+}", handlers.CompanyUpdate(app.App)).Methods(http.MethodPatch)
	router.HandleFunc("/{company_id:[0-9]+}", handlers.CompanyDelete(app.App)).Methods(http.MethodDelete)
	router.HandleFunc("/search", handlers.CompanySearch(app.App)).Methods(http.MethodPost)
}
