package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/api/http/handlers"
)

func (app *App) SetLoadingPlanRoutes(router *mux.Router) {
	router.HandleFunc("", handlers.LoadingPlanCreate(app.App)).Methods(http.MethodPost)
	router.HandleFunc("/{loading_plans_id:[0-9]+}", handlers.LoadingPlanUpdate(app.App)).Methods(http.MethodPatch)
	router.HandleFunc("/{loading_plans_id:[0-9]+}", handlers.LoadingPlanDelete(app.App)).Methods(http.MethodDelete)
	router.HandleFunc("/search", handlers.LoadingPlanSearch(app.App)).Methods(http.MethodPost)
}
