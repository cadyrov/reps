package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/api/http/handlers"
)

func (app *App) SetObjectDestinationRoutes(router *mux.Router) {
	router.HandleFunc("/create", handlers.ObjectDestinationSearch(app.App)).Methods(http.MethodPost)
	router.HandleFunc("", handlers.ObjectDestinationCreate(app.App)).Methods(http.MethodPost)
	router.HandleFunc("/{destination_id:[0-9]+}",
		handlers.ObjectDestinationUpdate(app.App)).Methods(http.MethodPatch)
	router.HandleFunc("/{destination_id:[0-9]+}",
		handlers.ObjectDestinationDelete(app.App)).Methods(http.MethodDelete)
}
