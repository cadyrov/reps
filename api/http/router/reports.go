package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/api/http/handlers"
)

func (app *App) SetReportsRoutes(router *mux.Router) {
	router.HandleFunc("/{report_id:[0-9]+}/file/upload",
		handlers.ReportFileUpload(app.App)).Methods(http.MethodPost)
	router.HandleFunc("/{report_id:[0-9]+}/file",
		handlers.ReportFileList(app.App)).Methods(http.MethodGet)
	router.HandleFunc("/{report_id:[0-9]+}/file/drop",
		handlers.ReportFileDrop(app.App)).Methods(http.MethodPost)
	router.HandleFunc("", handlers.ReportsSearch(app.App)).Methods(http.MethodPost)
	router.HandleFunc("/{report_id:[0-9]+}/constructions",
		handlers.ReportsConstructions(app.App)).Methods(http.MethodPost)
	router.HandleFunc("/{report_id:[0-9]+}/plans",
		handlers.ReportsPlans(app.App)).Methods(http.MethodGet)
	router.HandleFunc("/{report_id:[0-9]+}/uploadphoto",
		handlers.ReportUploadPhoto(app.App)).Methods(http.MethodPost)
	router.HandleFunc("/{report_id:[0-9]+}/blocks",
		handlers.ReportBlocks(app.App)).Methods(http.MethodGet)
	router.HandleFunc("/{report_id:[0-9]+}/uploadplan",
		handlers.ReportUploadPlan(app.App)).Methods(http.MethodPost)
}
