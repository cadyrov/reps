package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/techconnn/reports/api/http/handlers"
)

func (app *App) SetSystemRoutes(router *mux.Router) {
	router.HandleFunc("/health", handlers.HealthCheck()).Methods(http.MethodGet)
	router.HandleFunc("/swagger", handlers.Swagger()).Methods(http.MethodGet)
}
