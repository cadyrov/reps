package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"

	"github.com/gorilla/mux"
	"github.com/jessevdk/go-flags"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/techconnn/reports/api/http/router"
	"gitlab.com/techconnn/reports/domain"
)

func main() {
	ctxb := context.Background()

	cnf, srv, app := initServer(ctxb)

	go func() {
		rt := mux.NewRouter()
		rt.Handle("/metrics", promhttp.Handler())

		srv := &http.Server{
			Handler:           rt,
			Addr:              ":9000",
			ReadTimeout:       app.Config.Web.ReadTimeout,
			WriteTimeout:      app.Config.Web.WriteTimeout,
			ReadHeaderTimeout: app.Config.Web.ReadTimeout,
			IdleTimeout:       app.Config.Web.IdleTimeout,
		}

		if err := srv.ListenAndServe(); err != nil {
			log.Err(err).Msg("metrics")
		}
	}()

	go func() {
		if cnf.Web.SSLSertPath != "" && cnf.Web.SSLKeyPath != "" {
			if err := srv.ListenAndServeTLS(cnf.Web.SSLSertPath, cnf.Web.SSLKeyPath); err != nil {
				panic(err)
			}

			return
		}

		if err := srv.ListenAndServe(); err != nil {
			panic(err)
		}
	}()

	app.Logger.Warn().
		Str("host", app.Config.Web.Host).
		Int("port", app.Config.Web.Port).
		Msg("server start on")

	c := make(chan os.Signal, 1)

	signal.Notify(c, syscall.SIGTERM)

	<-c

	ctx, cancel := context.WithTimeout(ctxb, app.Config.Web.ReadTimeout)

	if err := srv.Shutdown(ctx); err != nil {
		cancel()

		panic(err)
	}

	app.Logger.Warn().Msg("Server stop")

	cancel()

	runtime.Goexit()
}

func initServer(ctx context.Context) (domain.Config, *http.Server, *router.App) {
	conf := domain.Config{}

	parser := flags.NewParser(&conf, flags.Default)
	if _, err := parser.Parse(); err != nil {
		fmt.Printf("error parse env: %s\n", err.Error())
		os.Exit(1)
	}

	logger := zerolog.New(os.Stdout)
	zerolog.SetGlobalLevel(conf.LogLevel)

	app := router.New(ctx, logger, conf)

	srv := &http.Server{
		Handler:           app.GetRoutes(),
		Addr:              app.Config.Web.Host + ":" + strconv.Itoa(app.Config.Web.Port),
		ReadTimeout:       app.Config.Web.ReadTimeout,
		ReadHeaderTimeout: app.Config.Web.ReadTimeout,
		WriteTimeout:      app.Config.Web.WriteTimeout,
		IdleTimeout:       app.Config.Web.IdleTimeout,
	}

	return conf, srv, app
}
