package domain

import "net/url"

type BlockType int

const (
	ConstructionBlockRoofID               BlockType = 10
	ConstructionBlockFacadeID             BlockType = 20
	ConstructionBlockBearingStructuresID  BlockType = 30
	ConstructionBlockBasementID           BlockType = 40
	ConstructionBlockFoundationsID        BlockType = 50
	ConstructionBlockEngineeringNetworkID BlockType = 60
	ConstructionBlockOtherID              BlockType = 990
)

func (bt BlockType) String() string {
	switch bt {
	case ConstructionBlockRoofID:
		return "Крыша и кровля"
	case ConstructionBlockFacadeID:
		return "Фасад"
	case ConstructionBlockBearingStructuresID:
		return "Несущие конструкции"
	case ConstructionBlockBasementID:
		return "Подвал"
	case ConstructionBlockFoundationsID:
		return "Фундамент"
	case ConstructionBlockEngineeringNetworkID:
		return "Инженерные сети"
	case ConstructionBlockOtherID:
		return "Прочее"
	default:
		return "Неизвестная конструкция"
	}
}

const defaultColor = "#0072C5"

func (bt BlockType) Color() string {
	switch bt {
	case ConstructionBlockRoofID:
		return defaultColor
	case ConstructionBlockFacadeID:
		return "#31B47D"
	case ConstructionBlockBearingStructuresID:
		return "#E04C40"
	case ConstructionBlockBasementID:
		return "#F17800"
	case ConstructionBlockFoundationsID:
		return "#00A8EA"
	case ConstructionBlockEngineeringNetworkID:
		return "#31B47D"
	case ConstructionBlockOtherID:
		return defaultColor
	default:
		return defaultColor
	}
}

func (bt BlockType) ImageURL() *url.URL {
	defaultURL := &url.URL{
		Scheme: "https",
		Host:   "storage.yandexcloud.net",
		Path:   "techcon-nn/construction_blocks/10.png",
	}

	switch bt {
	case ConstructionBlockRoofID:
		return defaultURL
	case ConstructionBlockFacadeID:
		return defaultURL
	case ConstructionBlockBearingStructuresID:
		return defaultURL
	case ConstructionBlockBasementID:
		return defaultURL
	case ConstructionBlockFoundationsID:
		return defaultURL
	case ConstructionBlockEngineeringNetworkID:
		return defaultURL
	case ConstructionBlockOtherID:
		return defaultURL
	default:
		return defaultURL
	}
}

type Block struct {
	ID       BlockType `json:"id"  binding:"required"`
	Name     string    `json:"name"  binding:"required"`
	Color    string    `json:"color"`
	ImageURL string    `json:"image_url"`
}

func NewBlock(t BlockType) Block {
	return Block{
		ID:       t,
		Name:     t.String(),
		Color:    t.Color(),
		ImageURL: t.ImageURL().String(),
	}
}

type Blocks []Block

func FullList() Blocks {
	return []Block{NewBlock(ConstructionBlockRoofID),
		NewBlock(ConstructionBlockFacadeID),
		NewBlock(ConstructionBlockBearingStructuresID),
		NewBlock(ConstructionBlockBasementID),
		NewBlock(ConstructionBlockFoundationsID),
		NewBlock(ConstructionBlockEngineeringNetworkID),
		NewBlock(ConstructionBlockOtherID),
	}
}
