package domain

import (
	validation "github.com/cadyrov/govalidation"
	"github.com/uptrace/bun"
)

type Client struct {
	bun.BaseModel `bun:"table:client,alias:c" json:"-"`

	ID        int  `bun:"id,pk,autoincrement" json:"id"    description:""`
	AccountID *int `bun:"account_id" json:"account_id"    description:""  `
	ClientForm
}

type ClientForm struct {
	Name              string  `bun:"name" json:"name"    description:""   binding:"required"`
	Address           *string `bun:"address" json:"address"    description:""  `
	Director          *string `bun:"director" json:"director"    description:""  `
	Accountant        *string `bun:"accountant" json:"accountant"    description:""  `
	TaxSystem         *int    `bun:"tax_system" json:"tax_system"    description:""  `
	Phone             *string `bun:"phone" json:"phone"    description:""  `
	Kpp               *string `bun:"kpp" json:"kpp"    description:""  `
	Inn               *string `bun:"inn" json:"inn"    description:""  `
	Ogrn              *string `bun:"ogrn" json:"ogrn"    description:""  `
	CorrespondentBill *string `bun:"correspondent_bill" json:"correspondent_bill"    description:""  `
	PaymentBill       *string `bun:"payment_bill" json:"payment_bill"    description:""  `
	Bank              *string `bun:"bank" json:"bank"    description:""  `
	Bik               *string `bun:"bik" json:"bik"    description:""  `
	Email             *string `bun:"email" json:"email"    description:""  `
}

type Clients []Client

func (c Client) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Name, validation.Required, validation.Length(1, VarcharSQLLimit)),
	)
}

type ClientLookupForm struct {
	AccountID int `json:"-"`
	SearchForm
}
