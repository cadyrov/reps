package domain

import (
	"time"

	validation "github.com/cadyrov/govalidation"
	"github.com/uptrace/bun"
)

type ClimateCenter struct {
	bun.BaseModel   `bun:"table:climate_center,alias:cc" json:"-"`
	ID              int        `bun:"id,pk,autoincrement" json:"id"    description:""   binding:"required"`
	AccountID       *int       `bun:"account_id" json:"account_id"    description:""  `
	ParentAccountID *int       `bun:"parent_account_id" json:"parent_account_id"    description:""  `
	OldID           *int       `bun:"old_id" json:"old_id"    description:""  `
	DateDelete      *time.Time `bun:"date_delete" json:"date_delete"    description:""  `

	ClimateCenterForm
}

// nolint: lll
type ClimateCenterForm struct {
	Name string `bun:"name" json:"name"    description:""   binding:"required"`
}

type ClimateCenters []ClimateCenter

func (m ClimateCenter) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.Name, validation.Required, validation.Length(1, VarcharSQLLimit)),
	)
}

type ClimateCenterLookupForm struct {
	AccountID int `json:"-"`
	SearchForm
}
