package domain

import (
	"time"

	validation "github.com/cadyrov/govalidation"
	"github.com/uptrace/bun"
)

type ClimateParameter struct {
	bun.BaseModel   `bun:"table:climate_parameter,alias:cp" json:"-"`
	ID              int        `bun:"id,pk,autoincrement" json:"id"    description:""   binding:"required"`
	AccountID       *int       `bun:"account_id" json:"account_id"    description:""  `
	ParentAccountID *int       `bun:"parent_account_id" json:"parent_account_id"    description:""  `
	OldID           *int       `bun:"old_id" json:"old_id"    description:""  `
	DateDelete      *time.Time `bun:"date_delete" json:"date_delete"    description:""  `

	ClimateParameterForm
}

type ClimateParameterForm struct {
	Name            string  `bun:"name" json:"name"    description:""   binding:"required"`
	Value           *string `bun:"value" json:"value"    description:""  `
	Document        *string `bun:"document" json:"document"    description:""  `
	ClimateCenterID *int    `bun:"climate_center_id" json:"climate_center_id"    description:""  `
}

type ClimateParameters []ClimateParameter

func (m ClimateParameter) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.Name, validation.Required, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&m.ClimateCenterID, validation.Required),
	)
}

type ClimateParameterLookupForm struct {
	AccountID       int `json:"-"`
	ClimateCenterID int `json:"climate_center_id"`
	SearchForm
}
