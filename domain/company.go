package domain

import (
	validation "github.com/cadyrov/govalidation"
	"github.com/uptrace/bun"
)

type Company struct {
	bun.BaseModel `bun:"table:company,alias:lp" json:"-"`
	ID            int `bun:"id,pk,autoincrement" json:"id"    description:""   binding:"required"`
	AccountID     int `bun:"account_id" json:"account_id"    description:""   binding:"required"`
	CompanyForm
}

// nolint:misspell
type CompanyForm struct {
	Name                 string   `bun:"name" json:"name"    description:""   binding:"required"`
	Ceo                  *string  `bun:"ceo" json:"ceo"    description:""  `
	Address              *string  `bun:"address" json:"address"    description:""  `
	Engineer             *string  `bun:"engeneer" json:"engeneer"    description:""  `
	IsClosed             *int     `bun:"is_closed" json:"is_closed"    description:""  `
	Kpp                  *string  `bun:"kpp" json:"kpp"    description:""  `
	Bank                 *string  `bun:"bank" json:"bank"    description:""  `
	Bik                  *string  `bun:"bik" json:"bik"    description:""  `
	Ogrn                 *string  `bun:"ogrn" json:"ogrn"    description:""  `
	Inn                  *string  `bun:"inn" json:"inn"    description:""  `
	Phone                *string  `bun:"phone" json:"phone"    description:""  `
	TaxTypeID            *int     `bun:"tax_type_id" json:"tax_type_id"    description:""  `
	RasSchet             *string  `bun:"ras_schet" json:"ras_schet"    description:""  `
	CorSchet             *string  `bun:"cor_schet" json:"cor_schet"    description:""  `
	Booker               *string  `bun:"booker" json:"booker"    description:""  `
	FileMigrated         *bool    `bun:"file_migrated" json:"file_migrated"    description:""  `
	FileMigrationMessage *string  `bun:"file_migration_message" json:"file_migration_message"    description:""  `
	LogoHeaderFilePath   *string  `bun:"logo_header_file_path" json:"logo_header_file_path"    description:""  `
	LogoHeaderFileName   *string  `bun:"logo_header_file_name" json:"logo_header_file_name"    description:""  `
	LogoHeaderFileID     *int     `bun:"logo_header_file_id" json:"logo_header_file_id"    description:""  `
	LogoHeaderWidth      *float64 `bun:"logo_header_width" json:"logo_header_width"    description:""  `
	LogoHeaderHeight     *float64 `bun:"logo_header_height" json:"logo_header_height"    description:""  `
}

type Companys []Company

func (m Company) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.Name, validation.Required, validation.Length(1, VarcharSQLLimit)),
	)
}

type CompanyLookupForm struct {
	AccountID int `json:"-"`
	SearchForm
}
