package domain

import (
	"time"

	"github.com/rs/zerolog"
)

type Config struct {
	Project
	PSQL
	Web
	Auth
}

type Project struct {
	Name        string        `long:"project-name" env:"REPORTS_PROJECT_NAME" required:"true"`
	PhpURI      string        `long:"project-php-uri" env:"REPORTS_PROJECT_PHP_URI" required:"true"`
	TrashSizeMB int64         `long:"project-trash-size-mb" env:"REPORTS_PROJECT_TRASH_SIZE_MB" default:"100"`
	LogLevel    zerolog.Level `long:"project-log_level" env:"REPORTS_PROJECT_LOG_LEVEL" default:"1"`
}

type Auth struct {
	URI          string        `long:"auth-uru" env:"REPORTS_AUTH_URI" required:"true"`
	Timeout      time.Duration `long:"auth-duration" env:"REPORTS_AUTH_DURATION" required:"true"`
	XServerToken string        `long:"auth-x-server-token" env:"REPORTS_AUTH_X_SERVER_TOKEN" required:"true"`
}

// nolint:lll
type Web struct {
	Host                    string        `long:"web-host" env:"REPORTS_WEB_HOST"`
	Port                    int           `long:"web-port" env:"REPORTS_WEB_PORT"`
	ReadTimeout             time.Duration `long:"web-read-timeout" env:"REPORTS_WEB_READ_TIMEOUT"`
	WriteTimeout            time.Duration `long:"web-write-timeout" env:"REPORTS_WEB_WRITE_TIMEOUT"`
	IdleTimeout             time.Duration `long:"web-idle-timeout" env:"REPORTS_WEB_IDLE_TIMEOUT"`
	Cors                    string        `long:"web-cors" env:"REPORTS_WEB_CORS"`
	SSLSertPath             string        `long:"web-ssl-sert-path" env:"REPORTS_WEB_SSL_SERT_PATH"`
	SSLKeyPath              string        `long:"web-ssl-web" env:"REPORTS_WEB_SSL_KEY_PATH"`
	SSEWriteMessageDuration time.Duration `long:"web-sse-write-message-duration" env:"REPORTS_SSE_WRITE_MESSAGE_DURATION" default:"2s"`
}

type PSQL struct {
	DSN          string        `long:"repository-dsn" env:"REPORTS_PSQL_DSN"`
	ReadTimeout  time.Duration `long:"repository-read-timeout" env:"REPORTS_PSQL_READ_TIMEOUT"`
	WriteTimeout time.Duration `long:"repository-write-timeout" env:"REPORTS_PSQL_WRITE_TIMEOUT"`
}
