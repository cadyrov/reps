package domain

import (
	"time"

	"github.com/cadyrov/goerr"
	validation "github.com/cadyrov/govalidation"
	"github.com/uptrace/bun"
)

type Construction struct {
	bun.BaseModel `bun:"table:construction,alias:cn" json:"-"`

	ID              int        `bun:"id,pk,autoincrement" json:"id"    description:""  `
	DateDelete      *time.Time `bun:"date_delete" json:"date_delete"    description:""   binding:"required"`
	OldID           *int       `bun:"old_id" json:"old_id"    description:""   binding:"required"`
	ParentAccountID *int       `bun:"parent_account_id" json:"parent_account_id"    description:""   binding:"required"`
	FileID          *int       `bun:"file_id" json:"file_id"    description:""   binding:"required"`
	FilePath        *string    `bun:"file_path" json:"file_path"    description:""   binding:"required"`
	ActiveFileID    *int       `bun:"active_file_id" json:"active_file_id"    description:""   binding:"required"`
	ActiveFilePath  *string    `bun:"active_file_path" json:"active_file_path"    description:""   binding:"required"`
	Extension       *string    `bun:"extension" json:"extension"    description:""   binding:"required"`
	ActiveExtension *string    `bun:"active_extension" json:"active_extension"    description:""   binding:"required"`
	FileName        *string    `bun:"file_name" json:"file_name"    description:""   binding:"required"`
	ActiveFileName  *string    `bun:"active_file_name" json:"active_file_name"    description:""   binding:"required"`
	AccountID       int        `bun:"account_id" json:"account_id"    description:""  `
	ConstructionForm
}

// nolint: lll
type ConstructionForm struct {
	Name                string  `bun:"name" json:"name"    description:""  `
	SortRate            *int    `bun:"sort_rate" json:"sort_rate"    description:""   binding:"required"`
	ConstructionType    *int    `bun:"construction_type" json:"construction_type"    description:""   binding:"required"`
	ConstructionBlockID *int    `bun:"construction_block_id" json:"construction_block_id"    description:""   binding:"required"`
	Color               *string `bun:"color" json:"color"    description:""   binding:"required"`
}

type Constructions []Construction

type ConstructionLookupForm struct {
	AccountID int         `json:"-"`
	ReportID  int         `json:"-"`
	BlockIDs  []BlockType `json:"block_ids"`
	SearchForm
}

func (c Construction) Validate() goerr.IError {
	return validation.ValidateStruct(c,
		validation.Field(&c.Name, validation.Required, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&c.AccountID, validation.Required, validation.Min(1)),
		validation.Field(&c.Extension, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&c.ActiveExtension, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&c.FilePath, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&c.ActiveFilePath, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&c.FileName, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&c.ActiveFileName, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&c.ConstructionBlockID, validation.Required),
	)
}
