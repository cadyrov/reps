package domain

import "time"

type FileUploadForm struct {
	FileName string        `json:"file_name" binding:"required"`
	TTL      time.Duration `json:"-"`
}

type DropFileForm struct {
	Path string `json:"path" description:"Название файла" binding:"required"`
}
