package domain

import (
	validation "github.com/cadyrov/govalidation"
	"github.com/uptrace/bun"
)

// nolint:lll
type LoadingPlan struct {
	bun.BaseModel `bun:"table:loading_plans,alias:lp" json:"-"`

	ID                       int      `bun:"id,pk,autoincrement" json:"id"    description:"" `
	AccountID                *int     `bun:"account_id" json:"account_id"    description:""   binding:"required"`
	Hash                     *string  `bun:"hash" json:"hash"    description:""  `
	PlanFileID               *int     `bun:"plan_file_id" json:"plan_file_id"    description:""  `
	OriginalWidth            *float64 `bun:"original_width" json:"original_width"    description:""  `
	OriginalHeight           *float64 `bun:"original_height" json:"original_height"    description:""  `
	OriginalFileID           *int     `bun:"original_file_id" json:"original_file_id"    description:""  `
	OriginalFileName         *string  `bun:"original_file_name" json:"original_file_name"    description:""  `
	OriginalFilePath         *string  `bun:"original_file_path" json:"original_file_path"    description:""  `
	PlanFileName             *string  `bun:"plan_file_name" json:"plan_file_name"    description:""  `
	PlanFilePath             *string  `bun:"plan_file_path" json:"plan_file_path"    description:""  `
	BuiltPlanFileID          *int     `bun:"built_plan_file_id" json:"built_plan_file_id"    description:""  `
	BuiltPlanFileName        *string  `bun:"built_plan_file_name" json:"built_plan_file_name"    description:""  `
	BuiltPlanFilePath        *string  `bun:"built_plan_file_path" json:"built_plan_file_path"    description:""  `
	NeedBuild                bool     `bun:"need_build" json:"need_build"    description:""   binding:"required"`
	PlanFileMigrated         *bool    `bun:"plan_file_migrated" json:"plan_file_migrated"    description:""  `
	PlanFileMigrationMessage *string  `bun:"plan_file_migration_message" json:"plan_file_migration_message" description:""  `
	Width                    *float64 `bun:"width" json:"width"    description:""  `
	Height                   *float64 `bun:"height" json:"height"    description:""  `
	Extension                *string  `bun:"extension" json:"extension"    description:""  `
	TemplateFormat           *int     `bun:"template_format" json:"template_format"    description:""  `
	LoadingPlanForm
}

// nolint: lll
type LoadingPlanForm struct {
	Type     *int    `bun:"type" json:"type"    description:""  `
	Floors   *int    `bun:"floors" json:"floors"    description:""  `
	Position *int    `bun:"position" json:"position"    description:""  `
	ReportID *int    `bun:"report_id" json:"report_id"    description:""  `
	Name     *string `bun:"name" json:"name"    description:""  `
}

type LoadingPlans []LoadingPlan

func (m LoadingPlan) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.ReportID, validation.Required),
		validation.Field(&m.AccountID, validation.Required),
		validation.Field(&m.Type, validation.Required),
		validation.Field(&m.Name, validation.Required),
	)
}

type LoadingPlanLookupForm struct {
	AccountID int   `json:"-"`
	ReportID  int   `json:"-"`
	WithImage *bool `json:"with_image"` // фильтрует платы отдавая только с загруженными изображениями
	SearchForm
}
