package domain

import (
	"time"

	"github.com/uptrace/bun"
)

// ObjectDestination domain.
// nolint:lll
type ObjectDestination struct {
	bun.BaseModel `bun:"table:object_destination,alias:od" json:"-"`

	ID              int        `bun:"id,pk,autoincrement" json:"id"`
	AccountID       int        `bun:"account_id" json:"account_id" binding:"required"`
	ParentAccountID *int       `bun:"parent_account_id" json:"parent_account_id"`
	OldID           *int       `bun:"old_id" json:"old_id"`
	DeletedAt       *time.Time `bun:"date_delete" json:"date_delete"`
	ObjectDestinationForm
}

type ObjectDestinationForm struct {
	Name string `bun:"name" json:"name" binding:"required"`
}

type ObjectDestinations []ObjectDestination

type ObjectDestinationSearchForm struct {
	AccountID       []int `json:"-"`
	OldID           []int `json:"old_id"`
	ParentAccountID []int `json:"-"`
	IsDeleted       *bool `json:"-"`
	SearchForm
}
