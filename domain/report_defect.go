package domain

import "github.com/uptrace/bun"

type ReportDefect struct {
	bun.BaseModel `bun:"table:report_defect,alias:rd" json:"-"`

	ID        int `bun:"id,pk,autoincrement" json:"id"    description:"ИД"  `
	ReportID  int `bun:"report_id" json:"report_id"    description:""  `
	AccountID int `bun:"account_id" json:"account_id"    description:""  `
	ReportDefectForm
}

type ReportDefectState int

const (
	ReportDefectStateNew   = 10 // новый
	ReportDefectStateWork  = 20 // в работе
	ReportDefectStateRated = 30 // оценен
)

// nolint: lll
type ReportDefectForm struct {
	DefectID                *int              `bun:"defect_id" json:"defect_id"    description:""   binding:"required"`
	Location                *string           `bun:"location" json:"location"    description:"К удалению. Не используется."   binding:"required"`
	Status                  ReportDefectState `bun:"status" json:"status"    description:"Статус дефекта. Варианты: новый, в работе, оценено. В РАБОТЕ НЕ ИСПОЛЬЗУЕТСЯ, У ВСЕХ ДЕФЕКТОВ СТАТУС 10, т.е. НОВЫЙ"  `
	Floor                   *int              `bun:"floor" json:"floor"    description:"К удалению. Не используется."   binding:"required"`
	TechCondition           *string           `bun:"tech_condition" json:"tech_condition"    description:"Код состояния дефекта цифрой(10-90)"   binding:"required"`
	TechConditionCat        *string           `bun:"tech_condition_cat" json:"tech_condition_cat"    description:"Категория состояния дефекта, обозначаемая буквой (А-Г)"   binding:"required"`
	ReasonID                *int              `bun:"reason_id" json:"reason_id"    description:"ИД причины возникновения"   binding:"required"`
	FollowingID             *int              `bun:"following_id" json:"following_id"    description:"ИД вследствия возникновение"   binding:"required"`
	RecomendationID         *int              `bun:"recomendation_id" json:"recomendation_id"    description:"К удалению. Не используется."   binding:"required"`
	Recomendation           *string           `bun:"recomendation" json:"recomendation"    description:"К удалению. Не используется."   binding:"required"`
	ReasonName              *string           `bun:"reason_name" json:"reason_name"    description:"Наименование причины возникновения дефекта"   binding:"required"`
	FollowingName           *string           `bun:"following_name" json:"following_name"    description:"Наименование вследствия возникновения дефекта"   binding:"required"`
	ConstructionID          *int              `bun:"construction_id" json:"construction_id"    description:"ИД конструкции"   binding:"required"`
	ConstructionElementID   *int              `bun:"construction_element_id" json:"construction_element_id"    description:"ИД элемента конструкции. В ЛОГИКЕ НЕ ИСПОЛЬЗУЕТСЯ, ВО ВСЕХ ЗАПИСЯХ ОДНО ЗНАЧЕНИЕ - Элемент"   binding:"required"`
	PartID                  *int              `bun:"part_id" json:"part_id"    description:"К удалению. Не используется."   binding:"required"`
	ConstructionElementName *string           `bun:"construction_element_name" json:"construction_element_name"    description:"Наименование элемента конструкции. В ЛОГИКЕ НЕ ИСПОЛЬЗУЕТСЯ, ВЕЗДЕ ПОДСТАВЛЯЕТСЯ - Элемент"   binding:"required"`
	ElementName             *string           `bun:"element_name" json:"element_name"    description:"К удалению. Не используется."   binding:"required"`
	PartName                *string           `bun:"part_name" json:"part_name"    description:"К удалению. Не используется."   binding:"required"`
	MaterialID              *int              `bun:"material_id" json:"material_id"    description:"В логике не используется. Везде подставляется значение - Материал"   binding:"required"`
	TypeID                  *int              `bun:"type_id" json:"type_id"    description:"Тип измерения дефекта (площадь, объём, длина, ширина и т.д.)"   binding:"required"`
	DefectValue             *float32          `bun:"defect_value" json:"defect_value"    description:"Количественное значение дефекта(Есди измеряется в метрах, то 5 - значит 5 метров)"   binding:"required"`
	X                       *float64          `bun:"x" json:"x"    description:"Исходное значение координаты x расположения дефекта на плане. От 0 до 100. Логикой не изменяется."   binding:"required"`
	Y                       *float64          `bun:"y" json:"y"    description:"Исходное значение координаты y расположения дефекта на плане. От 0 до 100. Логикой не изменяется."   binding:"required"`
	LoadingPlansID          *int              `bun:"loading_plans_id" json:"loading_plans_id"    description:""   binding:"required"`
	X1                      *float64          `bun:"x1" json:"x1"    description:"Изменяемое значение координаты x расположения дефекта на плане. От 0 до 100. Используется в печати и для отображения на фронте."   binding:"required"`
	Y1                      *float64          `bun:"y1" json:"y1"    description:"Изменяемое значение координаты y расположения дефекта на плане. От 0 до 100. Используется в печати и для отображения на фронте."   binding:"required"`
	FootnotePosition        *int              `bun:"footnote_position" json:"footnote_position"    description:"Позиция положения линии, идущей от дефекта к его легенде на плане.(10-40)"   binding:"required"`
	Number                  *int              `bun:"number" json:"number"    description:"Номер дефекта. Может изменяться."   binding:"required"`
	FileID                  *int              `bun:"file_id" json:"file_id"    description:"ИД файла с фото в старой системе"   binding:"required"`
	FileName                *string           `bun:"file_name" json:"file_name"    description:"Наименование файла с фото"   binding:"required"`
	FilePath                *string           `bun:"file_path" json:"file_path"    description:"Путь к файлу с фото"   binding:"required"`
	FileMigrated            bool              `bun:"file_migrated" json:"file_migrated"    description:"Cлужебный параметр для миграции на S3"   binding:"required"`
	FileMigrationMessage    *string           `bun:"file_migration_message" json:"file_migration_message"    description:"Cлужебный параметр для миграции на S3"   binding:"required"`
	OriginalName            *string           `bun:"original_name" json:"original_name"    description:"Исходное наименование файла при загрузке с телефона"   binding:"required"`
	XLegend                 *float64          `bun:"x_legend" json:"x_legend"    description:"Координата x левой верхней точки легенды дефекта."   binding:"required"`
	YLegend                 *float64          `bun:"y_legend" json:"y_legend"    description:"Координата y левой верхней точки легенды дефекта"   binding:"required"`
	Width                   *float64          `bun:"width" json:"width"    description:"Ширина фото дефекта"   binding:"required"`
	Height                  *float64          `bun:"height" json:"height"    description:"Высота фото дефекта"   binding:"required"`
	FixDate                 *string           `bun:"fix_date" json:"fix_date"    description:"Сроки устранения дефекта (строкой)"   binding:"required"`
	Longitude               *float64          `bun:"longitude" json:"longitude"    description:"Долгота"   binding:"required"`
	Latitude                *float64          `bun:"latitude" json:"latitude"    description:"Широта"   binding:"required"`
	Xangle                  *float64          `bun:"xangle" json:"xangle"    description:"Угол наклона к X"   binding:"required"`
	Yangle                  *float64          `bun:"yangle" json:"yangle"    description:"Угол наклона к Y"   binding:"required"`
	Zangle                  *float64          `bun:"zangle" json:"zangle"    description:"Угол наклона к Z"   binding:"required"`
	LongitudeNet            *float64          `bun:"longitude_net" json:"longitude_net"    description:"Альтернативным образом измеренная долгота"   binding:"required"`
	LatitudeNet             *float64          `bun:"latitude_net" json:"latitude_net"    description:"альтернативным образом измеренная широта"   binding:"required"`
	XangleA                 *float64          `bun:"xangle_a" json:"xangle_a"    description:"Алтернативным образом измеренный угол наклона к X"   binding:"required"`
	YangleA                 *float64          `bun:"yangle_a" json:"yangle_a"    description:"Алтернативным образом измеренный угол наклона к Y"   binding:"required"`
	ZangleA                 *float64          `bun:"zangle_a" json:"zangle_a"    description:"Алтернативным образом измеренный угол наклона к Z"   binding:"required"`
	AudioFileID             *int              `bun:"audio_file_id" json:"audio_file_id"    description:"ИД аудиофайла"   binding:"required"`
	AudioFilePath           *string           `bun:"audio_file_path" json:"audio_file_path"    description:"Путь к аудиофайлу"   binding:"required"`
	Uname                   *string           `bun:"uname" json:"uname"    description:"К удалению. Не используется. Дублирует наименование загружаемого файла дефекта."   binding:"required"`
}

type ReportDefects []ReportDefect

func (t ReportDefect) Validate() error {
	return nil
}

type ReportDefectLookupForm struct {
	AccountID  int   `json:"-"`
	ReportID   int   `json:"-"`
	IsResizing *bool `json:"-"`
	SearchForm
}
