package domain

import (
	"time"

	"github.com/cadyrov/goerr"
	validation "github.com/cadyrov/govalidation"
	"github.com/uptrace/bun"
)

type (
	ReportState int
	ReportType  int
)

const (
	ReportTypeTechnicalConclusion ReportType = 10 // Заключение о техническом состоянии конструкций здания
	ReportTypeFond                ReportType = 20 // Форма фонда
	ReportTypeDefectList          ReportType = 30 // Дефектная ведомость

	ReportStateNew      ReportState = 10 // Новый
	ReportStatePhoto    ReportState = 20 // Обследование
	ReportStateMark     ReportState = 30 // Камералка
	ReportStateCheck    ReportState = 40 // Проверка
	ReportStateMatching ReportState = 50 // Согласование
	ReportStateArch     ReportState = 55 // Архив
	ReportStateBasket   ReportState = 60 // КОрзина
)

type Report struct {
	bun.BaseModel `bun:"table:report,alias:r" json:"-"`

	ID                   int         `bun:"id,pk,autoincrement" json:"id"`
	Address              string      `bun:"address" json:"address"  binding:"required"`
	CreateAt             time.Time   `bun:"create_at" json:"create_at"  binding:"required"`
	State                ReportState `bun:"state" json:"state"  binding:"required"`
	DeletedAt            *time.Time  `bun:"deleted_at" json:"deleted_at"`
	AccountID            int         `bun:"account_id" json:"account_id"  binding:"required"`
	ClerkID              *int        `bun:"clerk_id" json:"clerk_id"`
	PhotographerID       *int        `bun:"photographer_id" json:"photographer_id"`
	EngineerID           *int        `bun:"engineer_id" json:"engineer_id"`
	ClientID             *int        `bun:"client_id" json:"client_id"`
	CompanyID            *int        `bun:"company_id" json:"company_id"`
	ConstructionsJSON    *string     `bun:"constructions_json" json:"constructions_json"`
	ClimateCenterID      *int        `bun:"climate_center_id" json:"climate_center_id"`
	UPVSTableID          *int        `bun:"upvs_table_id" json:"upvs_table_id"`
	Criterion            *int        `bun:"criterion" json:"criterion"`
	WearJSON             *string     `bun:"wear_json" json:"wear_json"`
	WearUPVS             *string     `bun:"wear_upvs" json:"wear_upvs"`
	CapitalGroup         *int        `bun:"capital_group" json:"capital_group"`
	ReportStructure      *string     `bun:"report_structure" json:"report_structure"`
	CreatedUserID        *int        `bun:"created_user_id" json:"created_user_id"`
	FileMigrated         *bool       `bun:"file_migrated" json:"file_migrated"`
	FileMigrationMessage *string     `bun:"file_migration_message" json:"file_migration_message"`
	Type                 ReportType  `bun:"report_type" json:"report_type"  binding:"required"`
	CommonWear           *int        `bun:"common_wear" json:"common_wear"`
	Hash                 *string     `bun:"hash" json:"hash"`
	Liter                *string     `bun:"liter" json:"liter"`
	LiteraOld            *string     `bun:"litera_old" json:"litera_old"`
	Cipher               *string     `bun:"cipher" json:"cipher"`
	ObjectName           *string     `bun:"object_name" json:"object_name"`
	Name                 *string     `bun:"name" json:"name"`
}

func (r Report) Validate() goerr.IError {
	return validation.ValidateStruct(r,
		validation.Field(&r.Address, validation.Required, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&r.ConstructionsJSON, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&r.FileMigrationMessage, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&r.Hash, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&r.Liter, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&r.LiteraOld, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&r.Cipher, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&r.State, validation.Required, validation.Min(1),
			validation.In(ReportStateNew, ReportStatePhoto, ReportStateMark,
				ReportStateCheck, ReportStateMatching, ReportStateArch, ReportStateBasket)),
		validation.Field(&r.Type, validation.Required, validation.Min(1),
			validation.In(ReportTypeTechnicalConclusion, ReportTypeFond, ReportTypeDefectList)),
		validation.Field(&r.AccountID, validation.Required, validation.Min(1)),
		validation.Field(&r.CreatedUserID, validation.Required, validation.Min(1)),
		validation.Field(&r.CreateAt, validation.Required),
	)
}

type Reports []Report

type ReportSearchForm struct {
	// при пустых статусах вернутся отчеты во всех статусах
	States    []ReportState `json:"states"`
	AccountID int           `json:"-"`

	SearchForm
}
