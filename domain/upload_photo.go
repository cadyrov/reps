package domain

import (
	"fmt"

	validation "github.com/cadyrov/govalidation"
)

type UploadPhotoForm struct {
	ConstructionID int     `json:"construction_id" binding:"required"`
	LoadingPlanID  int     `json:"loading_plans_id" binding:"required"`
	X              float64 `json:"x"`
	Y              float64 `json:"y"`
	XAngle         float64 `json:"xangle"`
	YAngle         float64 `json:"yangle"`
	ZAngle         float64 `json:"zangle"`
	XAngleA        float64 `json:"xangle_a"`
	YAngleA        float64 `json:"yangle_a"`
	ZAngleA        float64 `json:"zangle_a"`
	Latitude       float64 `json:"latitude"`
	Longitude      float64 `json:"longitude"`
	LatitudeNET    float64 `json:"latitude_net"`
	// Абсолютное знание долготы
	LongitudeNET float64 `json:"longitude_net"`
	// Имя фото файла - уникально в пределах одного отчета
	PhotoFileName string `json:"photo_file_name" binding:"required"`
	// Имя аудио файла - уникально в пределах одного отчета
	AudioFileName string `json:"audio_file_name"`
	DefectName    string `json:"defect_name"`
}

type UploadPhotoRender struct {
	PhotoFileUploadLink string `json:"photo_file_upload_link" binding:"required"`
	AudioFileUploadLink string `json:"audio_file_upload_link"`
}

func (u UploadPhotoForm) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.ConstructionID, validation.Required, validation.Min(1)),
		validation.Field(&u.LoadingPlanID, validation.Required, validation.Min(1)),
		validation.Field(&u.X, validation.Required),
		validation.Field(&u.Y, validation.Required),
		validation.Field(&u.PhotoFileName, validation.Required, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&u.AudioFileName, validation.Length(1, VarcharSQLLimit)),
		validation.Field(&u.DefectName, validation.Required, validation.Length(1, VarcharSQLLimit)),
	)
}

func UploadDefectPhotoPath(reportID, defectID int, filename string) string {
	return fmt.Sprintf("%d/%d/images/%s", reportID, defectID, filename)
}

func UploadDefectOriginPhotoPath(reportID, defectID int, filename string) string {
	return fmt.Sprintf("%d/%d/images/origin/%s", reportID, defectID, filename)
}

func UploadAudioPhotoPath(reportID, defectID int, filename string) string {
	return fmt.Sprintf("%d/%d/audio/%s", reportID, defectID, filename)
}
