package domain

import (
	"fmt"

	validation "github.com/cadyrov/govalidation"
)

type UploadPlanPhotoForm struct {
	PlanID        int    `json:"plan_id" binding:"required"`
	PhotoFileName string `json:"photo_file_name" binding:"required"`
}

type UploadPlanPhotoRender struct {
	PlanPhotoUploadLink string `json:"plan_photo_upload_link" binding:"required"`
}

func (u UploadPlanPhotoForm) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.PlanID, validation.Required),
		validation.Field(&u.PhotoFileName, validation.Required, validation.Length(1, VarcharSQLLimit)),
	)
}

func UploadPlanPhotoPath(reportID, planID int, filename string) string {
	return fmt.Sprintf("%d/plans/%d/%s", reportID, planID, filename)
}
