// nolint:unused
package cache

import (
	"context"
	"errors"
	"time"

	"github.com/go-redis/redis/v8"
)

type Cache struct {
	client *redis.Client
	prefix string
}

func New(client *redis.Client, prefix string) *Cache {
	return &Cache{
		client: client,
		prefix: prefix,
	}
}

var ErrPing = errors.New("failed ping redis. Redis client isn't initialized")

func (r *Cache) Ping(ctx context.Context) error {
	if r.client == nil {
		return ErrPing
	}

	_, err := r.client.Ping(ctx).Result()

	return err
}

var ErrGet = errors.New("failed get key redis")

func (r *Cache) get(ctx context.Context, key string) (string, error) {
	if r.client == nil {
		return "", ErrGet
	}

	key = r.resolvePrefix(key)

	return r.client.Get(ctx, key).Result()
}

var ErrSet = errors.New("failed set key redis")

func (r *Cache) set(ctx context.Context, key string, value interface{}, ttl time.Duration) error {
	if r.client == nil {
		return ErrSet
	}

	key = r.resolvePrefix(key)

	return r.client.Set(ctx, key, value, ttl).Err()
}

var ErrInvalidate = errors.New("failed invalidate keys by pattern")

// nolint:unused
func (r *Cache) invalidate(ctx context.Context, pattern string) error {
	if r.client == nil {
		return ErrInvalidate
	}

	p := r.resolvePrefix(pattern)

	result := r.client.Keys(ctx, p)
	if result.Err() != nil {
		return ErrInvalidate
	}

	keys := result.Val()
	if len(keys) == 0 {
		return nil
	}

	return r.client.Del(ctx, keys...).Err()
}

func (r *Cache) resolvePrefix(key string) string {
	if len(key) > 0 {
		return r.prefix + ":" + key
	}

	return r.prefix
}

func (r *Cache) deleteKey(ctx context.Context, pattern string) error {
	return r.client.Del(ctx, r.resolvePrefix(pattern)).Err()
}
