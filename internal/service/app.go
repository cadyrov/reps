package service

import (
	"context"
	"log"
	"net/http"
	"runtime/debug"
	"time"

	"github.com/cadyrov/goerr"
	"github.com/rs/zerolog"
	"gitlab.com/hof4eg/auth_client"
	authDomain "gitlab.com/hof4eg/auth_client/domain"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/internal/storage/psql"
)

type App struct {
	config     domain.Config
	repository *psql.Storage
	logger     zerolog.Logger
	authClient *auth_client.Client
}

func New(ctx context.Context, config domain.Config, repository *psql.Storage,
	logger zerolog.Logger, authClient *auth_client.Client) *App {
	app := &App{
		config:     config,
		repository: repository,
		logger:     logger,
		authClient: authClient,
	}

	tk := time.NewTicker(time.Minute)

	go func() {
		defer func() {
			if r := recover(); r != nil {
				log.Println(string(debug.Stack()))
			}
		}()

		for {
			select {
			case _, ok := <-tk.C:
				if !ok {
					return
				}

				app.logger.Info().Msg("start resizer")

				app.Resize(ctx)
			case <-ctx.Done():
				return
			}
		}
	}()

	return app
}

const scope = "reports"

func (app *App) Validate(ctx context.Context, token string) (authDomain.UserRender, error) {
	return app.authClient.Check(ctx, scope, token)
}

func UserRenderFrom(ctx context.Context) authDomain.UserRender {
	requestContext := ctx.Value(domain.ContextKeyUserRender)

	if requestContext == nil {
		return authDomain.UserRender{}
	}

	return requestContext.(authDomain.UserRender)
}

func (app *App) HasOne(ctx context.Context, roles ...string) goerr.IError {
	ctxUR := UserRenderFrom(ctx)

	if ctxUR.ID == 0 {
		return goerr.New("user_render_nil").HTTP(http.StatusForbidden)
	}

	if !ctxUR.HasOne(roles...) {
		return goerr.New("no_role").HTTP(http.StatusForbidden)
	}

	return nil
}
