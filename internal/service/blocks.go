package service

import (
	"context"

	"github.com/cadyrov/goerr"
	"gitlab.com/techconnn/reports/domain"
)

func (app *App) ReportBlocks(ctx context.Context, reportID int) (domain.Blocks, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	if _, err := app.grantedReport(ctx, reportID, ctxUR.AccountID); err != nil {
		return nil, domain.ForbidErr(err)
	}

	return domain.FullList(), nil
}
