package service

import (
	"context"
	"errors"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

func (app *App) ClientCreate(ctx context.Context, form domain.ClientForm) (domain.Client, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	cl := domain.Client{}
	cl.AccountID = utils.Ptr(ctxUR.AccountID)
	cl.ClientForm = form

	if err := cl.Validate(); err != nil {
		return cl, domain.BadErr(err)
	}

	cl, err := app.repository.ClientUpsert(ctx, cl, nil)
	if err != nil {
		return domain.Client{}, err
	}

	return cl, err
}

func (app *App) ClientUpdate(ctx context.Context, clientID int, form domain.ClientForm) (domain.Client, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	cl, err := app.repository.ClientByID(ctx, clientID, nil)
	if err != nil {
		return domain.Client{}, err
	}

	if cl.AccountID == nil && *cl.AccountID != ctxUR.AccountID {
		return domain.Client{}, domain.ForbidErr(ErrClientForbidden)
	}

	cl.ClientForm = form

	cl, err = app.repository.ClientUpsert(ctx, cl, nil)
	if err != nil {
		return domain.Client{}, err
	}

	return cl, nil
}

var ErrClientForbidden = errors.New("forbidden")

func (app *App) ClientDelete(ctx context.Context, clientID int) goerr.IError {
	ctxUR := UserRenderFrom(ctx)

	cl, err := app.repository.ClientByID(ctx, clientID, nil)
	if err != nil {
		return err
	}

	if cl.AccountID == nil && *cl.AccountID != ctxUR.AccountID {
		return domain.ForbidErr(ErrClientForbidden)
	}

	if err := app.repository.ClientDelete(ctx, cl.ID, nil); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func (app *App) ClientSearch(ctx context.Context, form domain.ClientLookupForm) (domain.Clients,
	godict.Pagination, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	form.AccountID = ctxUR.AccountID

	dmn, total, err := app.repository.ClientSearch(ctx, form, nil)
	if err != nil {
		return nil, godict.Pagination{}, err
	}

	return dmn, godict.Pagination{Total: total}, nil
}
