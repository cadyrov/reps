package service

import (
	"context"
	"errors"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

func (app *App) ClimateCenterCreate(ctx context.Context,
	form domain.ClimateCenterForm) (domain.ClimateCenter, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	md := domain.ClimateCenter{}
	md.AccountID = utils.Ptr(ctxUR.AccountID)
	md.ClimateCenterForm = form

	if err := md.Validate(); err != nil {
		return md, domain.BadErr(err)
	}

	md, err := app.repository.ClimateCenterUpsert(ctx, md, nil)
	if err != nil {
		return domain.ClimateCenter{}, err
	}

	return md, err
}

func (app *App) ClimateCenterUpdate(ctx context.Context, climateCenterID int,
	form domain.ClimateCenterForm) (domain.ClimateCenter, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	md, err := app.repository.ClimateCenterByID(ctx, climateCenterID, nil)
	if err != nil {
		return domain.ClimateCenter{}, err
	}

	if md.AccountID == nil && *md.AccountID != ctxUR.AccountID {
		return domain.ClimateCenter{}, domain.ForbidErr(ErrClimateCenterForbidden)
	}

	md.Name = form.Name

	if err := md.Validate(); err != nil {
		return md, domain.BadErr(err)
	}

	md, err = app.repository.ClimateCenterUpsert(ctx, md, nil)
	if err != nil {
		return domain.ClimateCenter{}, err
	}

	return md, nil
}

var ErrClimateCenterForbidden = errors.New("forbidden")

func (app *App) ClimateCenterDelete(ctx context.Context, climateCenterID int) goerr.IError {
	ctxUR := UserRenderFrom(ctx)

	md, err := app.repository.ClimateCenterByID(ctx, climateCenterID, nil)
	if err != nil {
		return err
	}

	if md.AccountID == nil && *md.AccountID != ctxUR.AccountID {
		return domain.ForbidErr(ErrClimateCenterForbidden)
	}

	if err := app.repository.ClimateCenterDelete(ctx, md.ID, nil); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func (app *App) ClimateCenterSearch(ctx context.Context, form domain.ClimateCenterLookupForm) (domain.ClimateCenters,
	godict.Pagination, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	form.AccountID = ctxUR.AccountID

	dmn, total, err := app.repository.ClimateCenterSearch(ctx, form, nil)
	if err != nil {
		return nil, godict.Pagination{}, err
	}

	return dmn, godict.Pagination{Total: total}, nil
}

func (app *App) validateAccessClimateCenter(ctx context.Context, id int) goerr.IError {
	ctxUR := UserRenderFrom(ctx)

	center, err := app.repository.ClimateCenterByID(ctx, id, nil)
	if err != nil {
		return domain.IntErr(err)
	}

	if center.ID == 0 || center.AccountID == nil || *center.AccountID != ctxUR.AccountID {
		return domain.ForbidErr(ErrClimateCenterForbidden)
	}

	return nil
}
