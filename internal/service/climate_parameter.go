package service

import (
	"context"
	"errors"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

func (app *App) ClimateParameterCreate(ctx context.Context,
	form domain.ClimateParameterForm) (domain.ClimateParameter, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	md := domain.ClimateParameter{}
	md.AccountID = utils.Ptr(ctxUR.AccountID)
	md.ClimateParameterForm = form

	if err := md.Validate(); err != nil {
		return md, domain.BadErr(err)
	}

	if err := app.validateAccessClimateCenter(ctx, *md.ClimateCenterID); err != nil {
		return md, err
	}

	md, err := app.repository.ClimateParameterUpsert(ctx, md, nil)
	if err != nil {
		return domain.ClimateParameter{}, err
	}

	return md, err
}

func (app *App) ClimateParameterUpdate(ctx context.Context, climateParameterID int,
	form domain.ClimateParameterForm) (domain.ClimateParameter, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	md, err := app.repository.ClimateParameterByID(ctx, climateParameterID, nil)
	if err != nil {
		return domain.ClimateParameter{}, err
	}

	if md.AccountID == nil && *md.AccountID != ctxUR.AccountID {
		return domain.ClimateParameter{}, domain.ForbidErr(ErrClimateParameterForbidden)
	}

	md.Name = form.Name
	md.Value = form.Value
	md.Document = form.Document
	md.ClimateCenterID = form.ClimateCenterID

	if err := md.Validate(); err != nil {
		return md, domain.BadErr(err)
	}

	if err := app.validateAccessClimateCenter(ctx, *md.ClimateCenterID); err != nil {
		return md, err
	}

	md, err = app.repository.ClimateParameterUpsert(ctx, md, nil)
	if err != nil {
		return domain.ClimateParameter{}, err
	}

	return md, nil
}

var ErrClimateParameterForbidden = errors.New("forbidden")

func (app *App) ClimateParameterDelete(ctx context.Context, climateParameterID int) goerr.IError {
	ctxUR := UserRenderFrom(ctx)

	md, err := app.repository.ClimateParameterByID(ctx, climateParameterID, nil)
	if err != nil {
		return err
	}

	if md.AccountID == nil && *md.AccountID != ctxUR.AccountID {
		return domain.ForbidErr(ErrClimateParameterForbidden)
	}

	if err := app.repository.ClimateParameterDelete(ctx, md.ID, nil); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func (app *App) ClimateParameterSearch(ctx context.Context,
	form domain.ClimateParameterLookupForm) (domain.ClimateParameters, godict.Pagination, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	form.AccountID = ctxUR.AccountID

	dmn, total, err := app.repository.ClimateParameterSearch(ctx, form, nil)
	if err != nil {
		return nil, godict.Pagination{}, err
	}

	return dmn, godict.Pagination{Total: total}, nil
}
