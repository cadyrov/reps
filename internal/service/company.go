package service

import (
	"context"
	"errors"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"gitlab.com/techconnn/reports/domain"
)

func (app *App) CompanyCreate(ctx context.Context, form domain.CompanyForm) (domain.Company, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	md := domain.Company{}
	md.AccountID = ctxUR.AccountID
	md.CompanyForm = form

	if err := md.Validate(); err != nil {
		return md, domain.BadErr(err)
	}

	md, err := app.repository.CompanyUpsert(ctx, md, nil)
	if err != nil {
		return domain.Company{}, err
	}

	return md, err
}

func (app *App) CompanyUpdate(ctx context.Context, companyID int,
	form domain.CompanyForm) (domain.Company, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	md, err := app.repository.CompanyByID(ctx, companyID, nil)
	if err != nil {
		return domain.Company{}, err
	}

	if md.AccountID != ctxUR.AccountID {
		return domain.Company{}, domain.ForbidErr(ErrCompanyForbidden)
	}

	md.CompanyForm = form

	if err := md.Validate(); err != nil {
		return md, domain.BadErr(err)
	}

	md, err = app.repository.CompanyUpsert(ctx, md, nil)
	if err != nil {
		return domain.Company{}, err
	}

	return md, nil
}

var ErrCompanyForbidden = errors.New("forbidden")

func (app *App) CompanyDelete(ctx context.Context, companyID int) goerr.IError {
	ctxUR := UserRenderFrom(ctx)

	md, err := app.repository.CompanyByID(ctx, companyID, nil)
	if err != nil {
		return err
	}

	if md.AccountID != ctxUR.AccountID {
		return domain.ForbidErr(ErrCompanyForbidden)
	}

	if err := app.repository.CompanyDelete(ctx, md.ID, nil); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func (app *App) CompanySearch(ctx context.Context, form domain.CompanyLookupForm) (domain.Companys,
	godict.Pagination, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	form.AccountID = ctxUR.AccountID

	dmn, total, err := app.repository.CompanySearch(ctx, form, nil)
	if err != nil {
		return nil, godict.Pagination{}, err
	}

	return dmn, godict.Pagination{Total: total}, nil
}
