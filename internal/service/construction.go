package service

import (
	"context"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"gitlab.com/techconnn/reports/domain"
)

func (app *App) ConstructionLookup(ctx context.Context, form domain.ConstructionLookupForm) (domain.Constructions,
	godict.Pagination, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	form.AccountID = ctxUR.AccountID

	if _, err := app.grantedReport(ctx, form.ReportID, ctxUR.AccountID); err != nil {
		return nil, godict.Pagination{}, domain.ForbidErr(err)
	}

	dmn, total, err := app.repository.ConstructionSearch(ctx, form, nil)
	if err != nil {
		return nil, godict.Pagination{}, err
	}

	return dmn, godict.Pagination{Total: total}, nil
}
