package service

import (
	"context"
	"errors"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

func (app *App) LoadingPlansLookup(ctx context.Context, form domain.LoadingPlanLookupForm) (domain.LoadingPlans,
	godict.Pagination, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	form.AccountID = ctxUR.AccountID

	if _, err := app.grantedReport(ctx, form.ReportID, ctxUR.AccountID); err != nil {
		return nil, godict.Pagination{}, domain.ForbidErr(err)
	}

	dmn, total, err := app.repository.LoadingPlanSearch(ctx, form, nil)
	if err != nil {
		return nil, godict.Pagination{}, err
	}

	return dmn, godict.Pagination{Total: total}, nil
}

func (app *App) LoadingPlanCreate(ctx context.Context, form domain.LoadingPlanForm) (domain.LoadingPlan, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	md := domain.LoadingPlan{}
	md.AccountID = utils.Ptr(ctxUR.AccountID)
	md.LoadingPlanForm = form

	if err := md.Validate(); err != nil {
		return md, domain.BadErr(err)
	}

	if _, err := app.grantedReport(ctx, *md.ReportID, ctxUR.AccountID); err != nil {
		return md, domain.ForbidErr(err)
	}

	md, err := app.repository.LoadingPlanUpsert(ctx, md, nil)
	if err != nil {
		return domain.LoadingPlan{}, err
	}

	return md, err
}

func (app *App) LoadingPlanUpdate(ctx context.Context, loadingPlansID int,
	form domain.LoadingPlanForm) (domain.LoadingPlan, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	md, err := app.repository.LoadingPlanByID(ctx, loadingPlansID, nil)
	if err != nil {
		return domain.LoadingPlan{}, err
	}

	if md.AccountID == nil && *md.AccountID != ctxUR.AccountID {
		return domain.LoadingPlan{}, domain.ForbidErr(ErrLoadingPlansForbidden)
	}

	md.LoadingPlanForm = form

	if err := md.Validate(); err != nil {
		return md, domain.BadErr(err)
	}

	if _, err := app.grantedReport(ctx, *md.ReportID, ctxUR.AccountID); err != nil {
		return md, domain.ForbidErr(err)
	}

	md, err = app.repository.LoadingPlanUpsert(ctx, md, nil)
	if err != nil {
		return domain.LoadingPlan{}, err
	}

	return md, nil
}

var ErrLoadingPlansForbidden = errors.New("forbidden")

func (app *App) LoadingPlanDelete(ctx context.Context, loadingPlanID int) goerr.IError {
	ctxUR := UserRenderFrom(ctx)

	md, err := app.repository.LoadingPlanByID(ctx, loadingPlanID, nil)
	if err != nil {
		return err
	}

	if md.AccountID == nil && *md.AccountID != ctxUR.AccountID {
		return domain.ForbidErr(ErrLoadingPlansForbidden)
	}

	if _, err := app.grantedReport(ctx, *md.ReportID, ctxUR.AccountID); err != nil {
		return domain.ForbidErr(err)
	}

	if err := app.repository.LoadingPlanDelete(ctx, md.ID, nil); err != nil {
		return domain.IntErr(err)
	}

	return nil
}
