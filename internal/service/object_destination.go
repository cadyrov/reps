package service

import (
	"context"
	"time"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/pkg/errors"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

func (app *App) ObjectDestinationSearch(ctx context.Context,
	form domain.ObjectDestinationSearchForm) (domain.ObjectDestinations, godict.Pagination, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	form.AccountID = []int{ctxUR.AccountID}
	form.IsDeleted = utils.Ptr(false)

	rs, total, err := app.repository.ObjectDestinationSearch(ctx, form, nil)
	if err != nil {
		return domain.ObjectDestinations{}, godict.Pagination{}, err
	}

	return rs, godict.Pagination{Total: total}, nil
}

func (app *App) ObjectDestinationCreate(ctx context.Context,
	form domain.ObjectDestinationForm) (domain.ObjectDestination, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	md := domain.ObjectDestination{
		AccountID:             ctxUR.AccountID,
		ObjectDestinationForm: form,
	}

	rs, err := app.repository.ObjectDestinationUpsert(ctx, md, nil)
	if err != nil {
		return domain.ObjectDestination{}, err
	}

	return rs, nil
}

var ErrODForbidden = errors.New("have not access")

func (app *App) ObjectDestinationUpdate(ctx context.Context,
	objectDestinationID int, form domain.ObjectDestinationForm) (domain.ObjectDestination, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	rs, err := app.repository.ObjectDestinationByID(ctx, objectDestinationID, nil)
	if err != nil {
		return domain.ObjectDestination{}, err
	}

	if rs.AccountID != ctxUR.AccountID || rs.DeletedAt != nil {
		return domain.ObjectDestination{}, domain.ForbidErr(ErrODForbidden)
	}

	rs.ObjectDestinationForm = form

	rs, err = app.repository.ObjectDestinationUpsert(ctx, rs, nil)
	if err != nil {
		return domain.ObjectDestination{}, err
	}

	return rs, nil
}

func (app *App) ObjectDestinationDelete(ctx context.Context,
	objectDestinationID int) goerr.IError {
	ctxUR := UserRenderFrom(ctx)

	rs, err := app.repository.ObjectDestinationByID(ctx, objectDestinationID, nil)
	if err != nil {
		return err
	}

	if rs.AccountID != ctxUR.AccountID || rs.DeletedAt != nil {
		return domain.ForbidErr(ErrODForbidden)
	}

	rs.DeletedAt = utils.Ptr(time.Now())

	_, err = app.repository.ObjectDestinationUpsert(ctx, rs, nil)

	return err
}
