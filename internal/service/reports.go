package service

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	authDomain "gitlab.com/hof4eg/auth_client/domain"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

func pathReportFileTrash(reportID int64, filename string) string {
	return fmt.Sprintf("/%d/trash/%s", reportID, filename)
}

var (
	ErrTrashFull           = errors.New("report storage is full")
	ErrReportHaveNotAccess = errors.New("not enough permission")
)

func (app *App) ReportUploadFile(ctx context.Context, reportID int64,
	form domain.FileUploadForm) (string, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	if _, err := app.grantedReport(ctx, int(reportID), ctxUR.AccountID); err != nil {
		return "", domain.ForbidErr(err)
	}

	rsp, errs := app.ReportListFile(ctx, reportID, "")
	if errs != nil {
		return "", errs
	}

	var size int64

	for i := range rsp.FileInfos {
		size += rsp.FileInfos[i].Size
	}

	if size > app.config.TrashSizeMB<<domain.MBTwoDegree {
		return "", domain.ConfErr(ErrTrashFull)
	}

	upl, er := app.authClient.GetCredentialsForUpload(ctx,
		pathReportFileTrash(reportID, form.FileName), form.TTL)

	if er != nil {
		return "", domain.IntErr(er)
	}

	return upl.Data, nil
}

func (app *App) ReportListFile(ctx context.Context, reportID int64,
	path string) (authDomain.FileListResponse, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	if _, err := app.grantedReport(ctx, int(reportID), ctxUR.AccountID); err != nil {
		return authDomain.FileListResponse{}, domain.ForbidErr(err)
	}

	pth := strings.TrimLeft(pathReportFileTrash(reportID, strings.TrimLeft(path, "/")), "/")

	dst, errs := app.authClient.FileList(ctx, pth)
	if errs != nil {
		return dst, domain.IntErr(errs)
	}

	return dst, nil
}

func (app *App) ReportDropFile(ctx context.Context, reportID int64,
	filename string) goerr.IError {
	ctxUR := UserRenderFrom(ctx)

	if _, err := app.grantedReport(ctx, int(reportID), ctxUR.AccountID); err != nil {
		return domain.ForbidErr(err)
	}

	pth := strings.TrimLeft(pathReportFileTrash(reportID, strings.TrimLeft(filename, "/")), "/")

	if err := app.authClient.DropFile(ctx, pth); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func (app *App) ReportLookup(ctx context.Context, form domain.ReportSearchForm) (domain.Reports,
	godict.Pagination, goerr.IError) {
	ctxUR := UserRenderFrom(ctx)

	form.AccountID = ctxUR.AccountID

	dmn, total, err := app.repository.ReportSearch(ctx, form, nil)
	if err != nil {
		return nil, godict.Pagination{}, err
	}

	return dmn, godict.Pagination{Total: total}, nil
}

var (
	ErrReportInWrongState   = errors.New("report must be in photo status")
	ErrLoadingPlansNotFound = errors.New("loading plans not found")
)

func (app *App) ReportUploadPhoto(ctx context.Context, reportID int,
	form domain.UploadPhotoForm) (domain.UploadPhotoRender, goerr.IError) {
	rnd := domain.UploadPhotoRender{}

	if err := form.Validate(); err != nil {
		return rnd, domain.BadErr(err)
	}

	ctxUR := UserRenderFrom(ctx)

	// проверка отчета
	rp, err := app.grantedReport(ctx, reportID, ctxUR.AccountID)
	if err != nil {
		return rnd, domain.ForbidErr(err)
	}

	if rp.State != domain.ReportStatePhoto {
		return rnd, domain.ForbidErr(ErrReportInWrongState)
	}

	lp, err := app.repository.LoadingPlanByID(ctx, form.LoadingPlanID, nil)
	if err != nil || lp.AccountID == nil || *lp.AccountID != ctxUR.AccountID {
		return rnd, domain.BadErr(ErrLoadingPlansNotFound)
	}

	cs, err := app.repository.ConstructionByID(ctx, form.ConstructionID, nil)
	if err != nil || cs.AccountID != ctxUR.AccountID {
		return rnd, domain.BadErr(ErrLoadingPlansNotFound)
	}

	rd := domain.ReportDefect{
		ReportID:  reportID,
		AccountID: ctxUR.AccountID,
		ReportDefectForm: domain.ReportDefectForm{
			Status:         domain.ReportDefectStateNew,
			ConstructionID: utils.Ptr(form.ConstructionID),
			X:              utils.Ptr(form.X),
			Y:              utils.Ptr(form.Y),
			LoadingPlansID: utils.Ptr(form.LoadingPlanID),
			X1:             utils.Ptr(form.X),
			Y1:             utils.Ptr(form.Y),
			Longitude:      utils.Ptr(form.Longitude),
			Latitude:       utils.Ptr(form.Latitude),
			Xangle:         utils.Ptr(form.XAngle),
			Yangle:         utils.Ptr(form.YAngle),
			Zangle:         utils.Ptr(form.ZAngle),
			LongitudeNet:   utils.Ptr(form.LongitudeNET),
			LatitudeNet:    utils.Ptr(form.LatitudeNET),
			XangleA:        utils.Ptr(form.XAngleA),
			YangleA:        utils.Ptr(form.YAngleA),
			ZangleA:        utils.Ptr(form.ZAngleA),
			Uname:          utils.Ptr(form.DefectName),
		},
	}

	rd, err = app.repository.ReportDefectUpsert(ctx, rd, nil)
	if err != nil {
		return rnd, domain.IntErr(err)
	}

	photo, err := app.authClient.GetCredentialsForUpload(ctx,
		domain.UploadDefectPhotoPath(reportID, rd.ID, form.PhotoFileName), domain.FileUploadLinkTTL)
	if err != nil {
		return rnd, domain.IntErr(err)
	}

	rnd.PhotoFileUploadLink = photo.Data
	rd.FilePath = utils.Ptr(photo.URL)
	rd.FileName = utils.Ptr(form.PhotoFileName)

	if form.AudioFileName != "" {
		audio, err := app.authClient.GetCredentialsForUpload(ctx,
			domain.UploadAudioPhotoPath(reportID, rd.ID, form.AudioFileName), domain.FileUploadLinkTTL)
		if err != nil {
			return rnd, domain.IntErr(err)
		}

		rnd.AudioFileUploadLink = audio.Data
		rd.AudioFilePath = utils.Ptr(audio.URL)
	}

	rd, err = app.repository.ReportDefectUpsert(ctx, rd, nil)
	if err != nil {
		return rnd, domain.IntErr(err)
	}

	return rnd, nil
}

func (app *App) ReportUploadPlan(ctx context.Context, reportID int,
	form domain.UploadPlanPhotoForm) (domain.UploadPlanPhotoRender, goerr.IError) {
	rnd := domain.UploadPlanPhotoRender{}

	if err := form.Validate(); err != nil {
		return rnd, domain.BadErr(err)
	}

	ctxUR := UserRenderFrom(ctx)

	// проверка отчета
	_, err := app.grantedReport(ctx, reportID, ctxUR.AccountID)
	if err != nil {
		return rnd, domain.ForbidErr(err)
	}

	lp, err := app.repository.LoadingPlanByID(ctx, form.PlanID, nil)
	if err != nil || lp.AccountID == nil || *lp.AccountID != ctxUR.AccountID || *lp.ReportID != reportID {
		return rnd, domain.BadErr(ErrLoadingPlansNotFound)
	}

	audio, err := app.authClient.GetCredentialsForUpload(ctx,
		domain.UploadPlanPhotoPath(reportID, lp.ID, form.PhotoFileName), domain.FileUploadLinkTTL)
	if err != nil {
		return rnd, domain.IntErr(err)
	}

	rnd.PlanPhotoUploadLink = audio.Data
	lp.PlanFilePath = utils.Ptr(audio.URL)

	_, err = app.repository.LoadingPlanUpsert(ctx, lp, nil)
	if err != nil {
		return rnd, domain.IntErr(err)
	}

	return rnd, nil
}

func (app *App) grantedReport(ctx context.Context, reportID, accountID int) (domain.Report, error) {
	rp, err := app.repository.ReportByID(ctx, reportID, nil)
	if err != nil {
		return domain.Report{}, ErrReportHaveNotAccess
	}

	if rp.AccountID != accountID {
		return domain.Report{}, ErrReportHaveNotAccess
	}

	return rp, nil
}
