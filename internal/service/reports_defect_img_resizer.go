package service

import (
	"bytes"
	"context"
	"errors"
	"image"
	"image/jpeg"
	"io"
	"net/http"
	"sync"
	"time"

	"github.com/nfnt/resize"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

const (
	maxCapBuffer  = 2 << 25
	baseCapBuffer = 2 << 25
	withResize    = 2 << 10
	routines      = 2
	chanCap       = 2
)

//nolint:gochecknoglobals
var bufferPool = sync.Pool{
	New: func() interface{} {
		buf := &bytes.Buffer{}

		buf.Grow(baseCapBuffer)

		return buf
	},
}

func (app *App) Resize(ctx context.Context) {
	// получить дефекты для ресайза
	defects, _, err := app.repository.ReportDefectSearch(ctx,
		domain.ReportDefectLookupForm{IsResizing: utils.Ptr(false)}, nil)
	if err != nil {
		app.logger.Err(err)

		return
	}

	app.logger.Info().Int("count", len(defects)).Msg("defects for resize")

	defCHan := make(chan domain.ReportDefect, chanCap)

	var wg sync.WaitGroup

	for i := 0; i < routines; i++ {
		wg.Add(1)

		go func() {
			defer wg.Done()

			for {
				select {
				case def, ok := <-defCHan:
					if !ok {
						return
					}

					app.resizeOneDefect(ctx, def)
				case <-ctx.Done():
					return
				}
			}
		}()
	}

	for i := range defects {
		defCHan <- defects[i]
	}

	close(defCHan)

	wg.Wait()
}

func (app *App) resizeOneDefect(ctx context.Context, defect domain.ReportDefect) {
	if defect.FilePath == nil || defect.FileName == nil {
		app.logger.Error().Interface("defect", defect).Msg("inconsistent")

		return
	}

	bucketPath := domain.UploadDefectPhotoPath(defect.ReportID, defect.ID, *defect.FileName)
	originPath := domain.UploadDefectOriginPhotoPath(defect.ReportID, defect.ID, *defect.FileName)

	rsp, err := app.authClient.FileList(ctx, bucketPath)
	if err != nil {
		app.logger.Err(err)

		return
	}

	app.logger.Trace().Interface("file response", rsp).
		Interface("defect", defect).Msg("FileList")

	if len(rsp.FileInfos) == 0 {
		app.logger.Error().Str("file", bucketPath).Int("defectID", defect.ID).Msg("Not found")

		return
	}

	app.logger.Debug().Str("file", bucketPath).Int("defectID", defect.ID).Msg("Founded")

	// скачать изображение
	bf := bufferPool.Get().(*bytes.Buffer)

	if err := downloadFileInBufferByLink(ctx, bf, *defect.FilePath); err != nil {
		app.logger.Err(err).Int("defectID", defect.ID).Msg("try to download file")

		return
	}

	// изменить размер
	originImage, _, err := image.Decode(bf)
	if err != nil {
		app.logger.Err(err).Int("defectID", defect.ID).Msg("try to decode image")

		defect.FileMigrated = true
		defect.FileMigrationMessage = utils.Ptr(err.Error())

		if _, err := app.repository.ReportDefectUpsert(ctx, defect, nil); err != nil {
			app.logger.Err(err).Int("defectID", defect.ID).Msg("try to update file")
		}

		return
	}

	newImage := resize.Resize(withResize, 0, originImage, resize.Lanczos3)

	resBuf := bufferPool.Get().(*bytes.Buffer)
	if err = jpeg.Encode(resBuf, newImage, nil); err != nil {
		app.logger.Err(err).Int("defectID", defect.ID).Msg("try to encode img file")

		defect.FileMigrated = true
		defect.FileMigrationMessage = utils.Ptr(err.Error())

		if _, err := app.repository.ReportDefectUpsert(ctx, defect, nil); err != nil {
			app.logger.Err(err).Int("defectID", defect.ID).Msg("try to update file")
		}

		return
	}

	if bf.Len() < maxCapBuffer {
		bf.Reset()

		bufferPool.Put(bf)
	}

	// скопировать в оригинал
	if err := app.authClient.CopyFile(ctx, bucketPath, originPath); err != nil {
		app.logger.Err(err).Int("defectID", defect.ID).Msg("try to copy origin file")

		return
	}

	// заменить версию файла
	if err := app.changeFileVersion(ctx, bucketPath, resBuf); err != nil {
		app.logger.Err(err).Int("defectID", defect.ID).Msg("try to change file file")

		return
	}

	if resBuf.Len() < maxCapBuffer {
		resBuf.Reset()

		bufferPool.Put(resBuf)
	}

	// пометить файл как обработанный
	defect.FileMigrated = true
	if _, err := app.repository.ReportDefectUpsert(ctx, defect, nil); err != nil {
		app.logger.Err(err).Int("defectID", defect.ID).Msg("try to update file")

		return
	}
}

var ErrTroublesS3 = errors.New("troubles with s3")

func (app *App) changeFileVersion(ctx context.Context, bucketPath string, reader io.Reader) error {
	cred, err := app.authClient.GetCredentialsForUpload(ctx, bucketPath, time.Minute)
	if err != nil {
		return err
	}

	rq, err := http.NewRequestWithContext(ctx, http.MethodPut, cred.Data, reader)
	if err != nil {
		return err
	}

	rsp, err := http.DefaultClient.Do(rq)
	if err != nil {
		return err
	}

	defer rsp.Body.Close()

	if rsp.StatusCode == http.StatusOK {
		return nil
	}

	return ErrTroublesS3
}

func downloadFileInBufferByLink(ctx context.Context, buffer *bytes.Buffer, url string) error {
	rq, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return err
	}

	rsp, err := http.DefaultClient.Do(rq)
	if err != nil {
		return err
	}

	defer rsp.Body.Close()

	_, err = buffer.ReadFrom(rsp.Body)
	if err != nil {
		return err
	}

	return nil
}
