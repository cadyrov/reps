package psql

import (
	"context"

	"gitlab.com/techconnn/reports/domain"
)

func (s *StoreSuite) TestSearchClient() {
	bks, cnt, err := s.service.ClientSearch(context.Background(),
		domain.ClientLookupForm{
			AccountID: 1,
			SearchForm: domain.SearchForm{
				ExcludedIDs: []int64{1},
				IDs:         []int64{2},
				Limit:       1,
				Query:       "jf",
			},
		}, nil)

	s.Require().Nil(err)

	_, _ = cnt, bks
}
