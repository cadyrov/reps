package psql

import (
	"context"
	"errors"

	"github.com/cadyrov/goerr"
	"github.com/lib/pq"
	"github.com/uptrace/bun"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

var ErrClimateParameterNotFound = errors.New("ClimateParameter not found")

func (s *Storage) ClimateParameterByID(ctx context.Context, id int,
	tx bun.IDB) (domain.ClimateParameter, goerr.IError) {
	md := domain.ClimateParameter{}

	if tx == nil {
		tx = s.db
	}

	if err := tx.NewSelect().Model(&md).Where("id = ?", id).Scan(ctx); err != nil {
		return md, domain.IntErr(err)
	}

	if md.ID == 0 {
		return md, domain.NotFoundErr(ErrClimateParameterNotFound)
	}

	return md, nil
}

func (s *Storage) ClimateParameterDelete(ctx context.Context, id int, tx bun.IDB) goerr.IError {
	if tx == nil {
		tx = s.db
	}

	if _, err := tx.NewDelete().Model(&domain.ClimateParameter{ID: id}).WherePK().Exec(ctx); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func (s *Storage) ClimateParameterUpsert(ctx context.Context, md domain.ClimateParameter,
	tx bun.IDB) (domain.ClimateParameter, goerr.IError) {
	if tx == nil {
		tx = s.db
	}

	if _, err := tx.NewInsert().
		Model(&md).
		On("CONFLICT (id) DO UPDATE").
		Exec(ctx); err != nil {
		return md, domain.IntErr(err)
	}

	return md, nil
}

func (s *Storage) ClimateParameterSearch(ctx context.Context, form domain.ClimateParameterLookupForm,
	tx bun.IDB) (domain.ClimateParameters, int, goerr.IError) {
	dm := domain.ClimateParameters{}

	if tx == nil {
		tx = s.db
	}

	q := tx.NewSelect().Model(&dm)

	if len(form.IDs) > 0 {
		q.Where("id = ANY(?)",
			pq.Array(form.IDs))
	}

	if len(form.ExcludedIDs) > 0 {
		q.Where("NOT (id = ANY(?))",
			pq.Array(form.ExcludedIDs))
	}

	if form.AccountID > 0 {
		q.Where("account_id = ?", form.AccountID)
	}

	if form.ClimateCenterID > 0 {
		q.Where("climate_center_id = ?", form.ClimateCenterID)
	}

	if len(form.Query) > 0 {
		q.Where("lower(name) like ?", utils.ToLike(&form.Query))
	}

	s.logger.Trace().Str("query", q.String()).Msg("report lookup request")

	cnt, err := q.Offset((form.Page - 1) * form.Limit).
		Limit(form.Limit).ScanAndCount(ctx)
	if err != nil {
		return dm, cnt, domain.IntErr(err)
	}

	return dm, cnt, nil
}
