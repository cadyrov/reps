package psql

import (
	"context"

	"gitlab.com/techconnn/reports/domain"
)

func (s *StoreSuite) TestSearchClimateParameters() {
	bks, cnt, err := s.service.ClimateParameterSearch(context.Background(),
		domain.ClimateParameterLookupForm{
			AccountID:       1,
			ClimateCenterID: 2,
			SearchForm: domain.SearchForm{
				ExcludedIDs: []int64{1},
				IDs:         []int64{2},
				Limit:       1,
				Query:       "jf",
			},
		}, nil)

	s.Require().Nil(err)

	_, _ = cnt, bks
}
