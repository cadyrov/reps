package psql

import (
	"context"

	"gitlab.com/techconnn/reports/domain"
)

func (s *StoreSuite) TestSearchCompany() {
	bks, cnt, err := s.service.CompanySearch(context.Background(),
		domain.CompanyLookupForm{
			AccountID: 1,
			SearchForm: domain.SearchForm{
				ExcludedIDs: []int64{1},
				IDs:         []int64{2},
				Limit:       1,
				Query:       "jf",
			},
		}, nil)

	s.Require().Nil(err)

	_, _ = cnt, bks
}
