package psql

import (
	"context"
	"errors"

	"github.com/cadyrov/goerr"
	"github.com/lib/pq"
	"github.com/uptrace/bun"
	"gitlab.com/techconnn/reports/domain"
)

var ErrConstructionNotFound = errors.New("construction not found")

func (s *Storage) ConstructionByID(ctx context.Context, id int,
	tx bun.IDB) (domain.Construction, goerr.IError) {
	md := domain.Construction{}

	if tx == nil {
		tx = s.db
	}

	if err := tx.NewSelect().Model(&md).Where("id = ?", id).Scan(ctx); err != nil {
		return md, domain.IntErr(err)
	}

	if md.ID == 0 {
		return md, domain.NotFoundErr(ErrConstructionNotFound)
	}

	return md, nil
}

func (s *Storage) ConstructionDelete(ctx context.Context, id int, tx bun.IDB) goerr.IError {
	if tx == nil {
		tx = s.db
	}

	if _, err := tx.NewDelete().Model(&domain.Construction{ID: id}).WherePK().Exec(ctx); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func (s *Storage) ConstructionUpsert(ctx context.Context, md domain.Construction,
	tx bun.IDB) (domain.Construction, goerr.IError) {
	if tx == nil {
		tx = s.db
	}

	if _, err := tx.NewInsert().
		Model(&md).
		On("CONFLICT (id) DO UPDATE").
		Exec(ctx); err != nil {
		return md, domain.IntErr(err)
	}

	return md, nil
}

func (s *Storage) ConstructionSearch(ctx context.Context, form domain.ConstructionLookupForm,
	tx bun.IDB) (domain.Constructions, int, goerr.IError) {
	dm := domain.Constructions{}

	if tx == nil {
		tx = s.db
	}

	q := tx.NewSelect().Model(&dm)

	if len(form.IDs) > 0 {
		q.Where("cn.id = ANY(?)",
			pq.Array(form.IDs))
	}

	if len(form.ExcludedIDs) > 0 {
		q.Where("NOT (cn.id = ANY(?))",
			pq.Array(form.ExcludedIDs))
	}

	if form.AccountID > 0 {
		q.Where("cn.account_id = ?", form.AccountID)
	}

	if form.ReportID != 0 {
		q.Join("JOIN construction_link_report AS clr").
			JoinOn("cn.id = clr.construction_id")
		q.Where("clr.report_id = ?", form.ReportID)
	}

	if len(form.BlockIDs) > 0 {
		q.Where("cn.construction_block_id = ANY(?)", pq.Array(form.BlockIDs))
	}

	q.Where("cn.date_delete is null")

	s.logger.Trace().Str("query", q.String()).Msg("report lookup request")

	cnt, err := q.Offset((form.Page - 1) * form.Limit).
		Limit(form.Limit).ScanAndCount(ctx)
	if err != nil {
		return dm, cnt, domain.IntErr(err)
	}

	return dm, cnt, nil
}
