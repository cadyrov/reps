package psql

import (
	"context"
	"errors"

	"github.com/cadyrov/goerr"
	"github.com/lib/pq"
	"github.com/uptrace/bun"
	"gitlab.com/techconnn/reports/domain"
)

var ErrLoadingPlanNotFound = errors.New("loading plans not found")

func (s *Storage) LoadingPlanByID(ctx context.Context, id int,
	tx bun.IDB) (domain.LoadingPlan, goerr.IError) {
	md := domain.LoadingPlan{}

	if tx == nil {
		tx = s.db
	}

	if err := tx.NewSelect().Model(&md).Where("id = ?", id).Scan(ctx); err != nil {
		return md, domain.IntErr(err)
	}

	if md.ID == 0 {
		return md, domain.NotFoundErr(ErrLoadingPlanNotFound)
	}

	return md, nil
}

func (s *Storage) LoadingPlanDelete(ctx context.Context, id int, tx bun.IDB) goerr.IError {
	if tx == nil {
		tx = s.db
	}

	if _, err := tx.NewDelete().Model(&domain.LoadingPlan{ID: id}).WherePK().Exec(ctx); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func (s *Storage) LoadingPlanUpsert(ctx context.Context, md domain.LoadingPlan,
	tx bun.IDB) (domain.LoadingPlan, goerr.IError) {
	if tx == nil {
		tx = s.db
	}

	if _, err := tx.NewInsert().
		Model(&md).
		On("CONFLICT (id) DO UPDATE").
		Exec(ctx); err != nil {
		return md, domain.IntErr(err)
	}

	return md, nil
}

func (s *Storage) LoadingPlanSearch(ctx context.Context, form domain.LoadingPlanLookupForm,
	tx bun.IDB) (domain.LoadingPlans, int, goerr.IError) {
	dm := domain.LoadingPlans{}

	if tx == nil {
		tx = s.db
	}

	q := tx.NewSelect().Model(&dm)

	if len(form.IDs) > 0 {
		q.Where("id = ANY(?)",
			pq.Array(form.IDs))
	}

	if len(form.ExcludedIDs) > 0 {
		q.Where("NOT (id = ANY(?))",
			pq.Array(form.ExcludedIDs))
	}

	if form.AccountID > 0 {
		q.Where("account_id = ?", form.AccountID)
	}

	if form.ReportID != 0 {
		q.Where("report_id = ?", form.ReportID)
	}

	switch {
	case form.WithImage == nil:
	case *form.WithImage:
		q.Where("plan_file_path is not NULL")
	case !*form.WithImage:
		q.Where("plan_file_path is  NULL")
	}

	s.logger.Trace().Str("query", q.String()).Msg("report lookup request")

	cnt, err := q.Offset((form.Page - 1) * form.Limit).
		Limit(form.Limit).ScanAndCount(ctx)
	if err != nil {
		return dm, cnt, domain.IntErr(err)
	}

	return dm, cnt, nil
}
