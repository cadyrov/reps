package psql

import (
	"context"

	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

func (s *StoreSuite) TestSearchLoadingPlans() {
	bks, cnt, err := s.service.LoadingPlanSearch(context.Background(),
		domain.LoadingPlanLookupForm{
			AccountID: 1,
			ReportID:  2,
			WithImage: utils.Ptr(true),
			SearchForm: domain.SearchForm{
				ExcludedIDs: []int64{1},
				IDs:         []int64{2},
				Limit:       1,
				Query:       "jf",
			},
		}, nil)

	s.Require().Nil(err)

	_, _ = cnt, bks
}
