package psql

import (
	"context"
	"errors"

	"github.com/cadyrov/goerr"
	"github.com/lib/pq"
	"github.com/uptrace/bun"
	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

var ErrObjectDestinationNotFound = errors.New("object destination not found")

func (s *Storage) ObjectDestinationByID(ctx context.Context, id int,
	tx bun.IDB) (domain.ObjectDestination, goerr.IError) {
	md := domain.ObjectDestination{}

	if tx == nil {
		tx = s.db
	}

	if err := tx.NewSelect().Model(&md).Where("id = ?", id).Scan(ctx); err != nil {
		return md, domain.IntErr(err)
	}

	if md.ID == 0 {
		return md, domain.NotFoundErr(ErrObjectDestinationNotFound)
	}

	return md, nil
}

func (s *Storage) ObjectDestinationDelete(ctx context.Context, id int, tx bun.IDB) goerr.IError {
	if tx == nil {
		tx = s.db
	}

	if _, err := tx.NewDelete().Model(&domain.ObjectDestination{ID: id}).WherePK().Exec(ctx); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func (s *Storage) ObjectDestinationUpsert(ctx context.Context, md domain.ObjectDestination,
	tx bun.IDB) (domain.ObjectDestination, goerr.IError) {
	if tx == nil {
		tx = s.db
	}

	if _, err := tx.NewInsert().
		Model(&md).
		On("CONFLICT (id) DO UPDATE").
		Exec(ctx); err != nil {
		return md, domain.IntErr(err)
	}

	return md, nil
}

func (s *Storage) ObjectDestinationSearch(ctx context.Context, form domain.ObjectDestinationSearchForm,
	tx bun.IDB) (domain.ObjectDestinations, int, goerr.IError) {
	dm := domain.ObjectDestinations{}

	if tx == nil {
		tx = s.db
	}

	q := tx.NewSelect().Model(&dm)

	if form.Query != "" {
		q.Where("name LIKE ?",
			utils.ToLike(&form.Query))
	}

	if len(form.IDs) > 0 {
		q.Where("id = ANY(?)",
			pq.Array(form.IDs))
	}

	if len(form.ExcludedIDs) > 0 {
		q.Where("NOT (id = ANY(?))",
			pq.Array(form.ExcludedIDs))
	}

	if len(form.AccountID) > 0 {
		q.Where("account_id = ANY(?)",
			pq.Array(form.AccountID))
	}

	if len(form.ParentAccountID) > 0 {
		q.Where("parent_account_id = ANY(?)",
			pq.Array(form.ParentAccountID))
	}

	if len(form.OldID) > 0 {
		q.Where("old_id = ANY(?)",
			pq.Array(form.OldID))
	}

	switch {
	case form.IsDeleted == nil:
	case *form.IsDeleted:
		q.Where("date_delete IS NOT NULL")
	case !*form.IsDeleted:
		q.Where("date_delete IS NULL")
	}

	strg := q.String()
	_ = strg

	cnt, err := q.Offset((form.Page - 1) * form.Limit).
		Limit(form.Limit).ScanAndCount(ctx)
	if err != nil {
		return dm, cnt, domain.IntErr(err)
	}

	return dm, cnt, nil
}
