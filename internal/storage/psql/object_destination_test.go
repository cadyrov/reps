package psql

import (
	"context"

	"gitlab.com/techconnn/reports/domain"
	"gitlab.com/techconnn/reports/utils"
)

func (s *StoreSuite) TestSearchObjectDestination() {
	bks, cnt, err := s.service.ObjectDestinationSearch(context.Background(),
		domain.ObjectDestinationSearchForm{
			AccountID:       []int{1},
			OldID:           []int{1},
			ParentAccountID: []int{1},
			IsDeleted:       utils.Ptr(true),
			SearchForm:      domain.SearchForm{Limit: 1, Query: "jf"},
		}, nil)

	s.Require().Nil(err)

	_, _ = cnt, bks
}
