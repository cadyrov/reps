package psql

import (
	"context"
	"errors"

	"github.com/cadyrov/goerr"
	"github.com/lib/pq"
	"github.com/uptrace/bun"
	"gitlab.com/techconnn/reports/domain"
)

var ErrDefectNotFound = errors.New("report defect not found")

func (s *Storage) ReportDefectByID(ctx context.Context, id int,
	tx bun.IDB) (domain.ReportDefect, goerr.IError) {
	md := domain.ReportDefect{}

	if tx == nil {
		tx = s.db
	}

	if err := tx.NewSelect().Model(&md).Where("id = ?", id).Scan(ctx); err != nil {
		return md, domain.IntErr(err)
	}

	if md.ID == 0 {
		return md, domain.NotFoundErr(ErrDefectNotFound)
	}

	return md, nil
}

func (s *Storage) ReportDefectDelete(ctx context.Context, id int, tx bun.IDB) goerr.IError {
	if tx == nil {
		tx = s.db
	}

	if _, err := tx.NewDelete().Model(&domain.ReportDefect{ID: id}).WherePK().Exec(ctx); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func (s *Storage) ReportDefectUpsert(ctx context.Context, md domain.ReportDefect,
	tx bun.IDB) (domain.ReportDefect, goerr.IError) {
	if tx == nil {
		tx = s.db
	}

	if _, err := tx.NewInsert().
		Model(&md).
		On("CONFLICT (id) DO UPDATE").
		Exec(ctx); err != nil {
		return md, domain.IntErr(err)
	}

	return md, nil
}

func (s *Storage) ReportDefectSearch(ctx context.Context, form domain.ReportDefectLookupForm,
	tx bun.IDB) (domain.ReportDefects, int, goerr.IError) {
	dm := domain.ReportDefects{}

	if tx == nil {
		tx = s.db
	}

	q := tx.NewSelect().Model(&dm)

	if len(form.IDs) > 0 {
		q.Where("id = ANY(?)",
			pq.Array(form.IDs))
	}

	if len(form.ExcludedIDs) > 0 {
		q.Where("NOT (id = ANY(?))",
			pq.Array(form.ExcludedIDs))
	}

	if form.AccountID > 0 {
		q.Where("account_id = ?", form.AccountID)
	}

	if form.ReportID > 0 {
		q.Where("report_id = ?", form.ReportID)
	}

	if form.IsResizing != nil {
		q.Where("file_migrated = ?", form.IsResizing)
	}

	s.logger.Trace().Str("query", q.String()).Msg("report defect request")

	cnt, err := q.Offset((form.Page - 1) * form.Limit).
		Limit(form.Limit).ScanAndCount(ctx)
	if err != nil {
		return dm, cnt, domain.IntErr(err)
	}

	return dm, cnt, nil
}
