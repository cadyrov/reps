package psql

import (
	"context"

	"gitlab.com/techconnn/reports/domain"
)

func (s *StoreSuite) TestSearchDefect() {
	bks, cnt, err := s.service.ReportDefectSearch(context.Background(),
		domain.ReportDefectLookupForm{
			AccountID: 1,
			ReportID:  2,
			SearchForm: domain.SearchForm{
				ExcludedIDs: []int64{1},
				IDs:         []int64{2},
				Limit:       1,
				Query:       "jf",
			},
		}, nil)

	s.Require().Nil(err)

	_, _ = cnt, bks
}
