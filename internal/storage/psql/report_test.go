package psql

import (
	"context"

	"gitlab.com/techconnn/reports/domain"
)

func (s *StoreSuite) TestReportLookup() {
	bks, cnt, err := s.service.ReportSearch(context.Background(),
		domain.ReportSearchForm{
			AccountID:  1,
			States:     []domain.ReportState{domain.ReportStateBasket, domain.ReportStateCheck},
			SearchForm: domain.SearchForm{Limit: 1, Query: "jf"},
		}, nil)

	s.Require().Nil(err)

	_, _ = cnt, bks
}
