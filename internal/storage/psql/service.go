package psql

import (
	"context"
	"database/sql"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
	"gitlab.com/techconnn/reports/domain"
)

type Storage struct {
	readTimeout  time.Duration
	writeTimeout time.Duration
	dsn          string
	db           *bun.DB
	logger       *zerolog.Logger
}

var ErrSetDsn = errors.New("set dsn")

func (s *Storage) Ping(ctx context.Context) error {
	if err := s.db.PingContext(ctx); err != nil {
		return domain.IntErr(err)
	}

	return nil
}

func New(dsn string, read, write time.Duration, logger *zerolog.Logger) (*Storage, error) {
	if strings.Trim(dsn, " ") == "" {
		return nil, ErrSetDsn
	}

	storage := &Storage{dsn: dsn, readTimeout: read, writeTimeout: write, logger: logger}

	sqlDB := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn), pgdriver.WithDialTimeout(read)))

	db := bun.NewDB(sqlDB, pgdialect.New())

	storage.db = db

	if e := storage.Ping(context.Background()); e != nil {
		return nil, domain.IntErr(e)
	}

	return storage, nil
}

func (s *Storage) TX(ctx context.Context) (bun.Tx, error) {
	tx, err := s.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return bun.Tx{}, domain.IntErr(err)
	}

	return tx, nil
}
