package psql

import (
	"os"
	"testing"

	"github.com/jessevdk/go-flags"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/suite"
	"gitlab.com/techconnn/reports/domain"
)

type StoreSuite struct {
	suite.Suite
	service *Storage
}

func (s *StoreSuite) SetupTest() {
	conf := domain.Config{}
	lg := zerolog.New(os.Stdout)

	parser := flags.NewParser(&conf, flags.IgnoreUnknown)
	_, err := parser.Parse()
	s.Require().Nil(err)

	srv, err := New(conf.PSQL.DSN, conf.PSQL.ReadTimeout, conf.PSQL.WriteTimeout, &lg)
	s.Require().Nil(err)

	s.service = srv
}

func TestSuite(t *testing.T) {
	if testing.Short() {
		t.Skip("short mode")
	}

	t.Parallel()

	suite.Run(t, new(StoreSuite))
}
