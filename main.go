package reports

import _ "embed"

// @title           Swagger report API
// @version         1.0
// @description     This is a techcon api server.
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      api.reports.nn-techcon.ru:8080
// @BasePath  /api

// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization

// @securityDefinitions.apikey ServerAuth
// @in header
// @name X-SERVER-TOKEN

// @externalDocs.description  OpenAPI
// @externalDocs.url          https://swagger.io/resources/open-api/

// nolint
//
//go:embed docs/swagger.yaml
var Swagger []byte
