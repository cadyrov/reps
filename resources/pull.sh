#!/usr/bin/env bash 
user=
token=
registry=registry.gitlab.com/techconnn/reports:main
img=registry.gitlab.com/techconnn/reports
file=/var/reports/.env

export `cat ${file}`

docker login -u ${user} -p ${token} ${registry}
docker pull ${registry}

docker system prune -f

imagehashfromcontainer=$(docker inspect reports --format='{{.Image}}'| grep sha)
imgId=$(docker images | grep $img | awk '{print $3}')
imagehashfromregistry=$(docker inspect $imgId --format='{{.Id}}'| grep sha)

echo "imagehashfromcontainer" ${imagehashfromcontainer}
echo "imgId" ${imgId}
echo "imagehashfromregistry" ${imagehashfromregistry}

echo "imagehashfromregistry" $REPORTS_HTTP_PORT

if [ "$imagehashfromcontainer" != "$imagehashfromregistry" ]
then docker stop reports
docker system prune -f
docker run -d --name reports -v /etc/letsencrypt/:/etc/letsencrypt -p $REPORTS_WEB_PORT:$REPORTS_WEB_PORT -p 9000:9000 --env-file=${file} ${registry}
echo "Docker re-run"
exit
fi

if [ "$imagehashfromcontainer" == "" ]
then docker stop reports
docker system prune -f
docker run -d --name reports -v /etc/letsencrypt/:/etc/letsencrypt -p $REPORTS_WEB_PORT:$REPORTS_WEB_PORT  -p 9000:9000 --env-file=${file} ${registry}
echo "Docker re-run"
fi