package utils

import (
	"net/http"

	"github.com/cadyrov/goerr"
	"golang.org/x/crypto/bcrypt"
)

func Hash(data string) (hash string, e error) {
	hashed, err := bcrypt.GenerateFromPassword([]byte(data), bcrypt.DefaultCost)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	return string(hashed), e
}

func Compare(hash string, password string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return goerr.New(err.Error()).HTTP(http.StatusBadRequest)
	}

	return nil
}
