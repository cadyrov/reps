package utils

import (
	"encoding/base64"
	"math/rand"
	"time"

	"github.com/cadyrov/goerr"
)

func StringPtr(in string) *string {
	return &in
}

// nolint
func RString(length int) (string, error) {
	b := make([]byte, length)

	_, err := rand.Read(b)
	if err != nil {
		return "", goerr.New(err.Error())
	}

	return base64.URLEncoding.EncodeToString(b), nil
}

// nolint
func RandSeq(n int) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-")
	b := make([]rune, n)
	rand.Seed(time.Now().UnixNano())

	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	return string(b)
}
