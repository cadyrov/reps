package utils

import "time"

func TimePtr(in time.Time) *time.Time {
	return &in
}

func Ptr[V string | bool | time.Time | int | int64 | float64](in V) *V {
	return &in
}
